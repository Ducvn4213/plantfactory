package mobile.arghub.planfactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import mobile.arghub.planfactory.introduction.IntroActivity;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.User;
import mobile.arghub.planfactory.service.network.Config;
import mobile.arghub.planfactory.utils.Utils;
import okhttp3.internal.Util;

public class StartActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = null;


        String lastToken = Utils.getPreference(StartActivity.this, Config.USER_TOKEN_KEY);
        String userString = Utils.getPreference(StartActivity.this, Config.USER_KEY);


//        lastToken = "N2QzYTg4YzUyZjc2NmZiYmNiZTc3YjZjMjdiYmU5ZTA=";
//        userString = "aaa";


        if (lastToken == null || userString == null || userString.trim().isEmpty() || lastToken.trim().isEmpty()) {
            intent = new Intent(StartActivity.this, IntroActivity.class);
        }
        else {
            Gson gson = new Gson();
            User user = gson.fromJson(userString, User.class);

//            User user = new User();
//            user.user_phone = "01234567890";
//            user.user_first_name = "just";
//            user.user_last_name = "for test";
//            user.user_email = "justfortest@gmail.com";
//            user.user_avatar = null;
//            user.user_birthday = 0;
//            user.user_gender = 0;



            PFService service = PFService.getInstance();
            service.currentUser = user;
            service.currentAccessToken = lastToken;

            intent = new Intent(StartActivity.this, MainActivity.class);
        }

        startActivity(intent);
        finish();
    }
}
