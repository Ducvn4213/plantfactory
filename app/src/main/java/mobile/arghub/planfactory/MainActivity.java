package mobile.arghub.planfactory;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import mobile.arghub.planfactory.add_farm.AddFarmQRCodeActivity;
import mobile.arghub.planfactory.new_season.NewSeasonActivity;
import mobile.arghub.planfactory.registration.PhoneRegistrationActivity;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.adapter.FarmNavigationAdapter;
import mobile.arghub.planfactory.service.model.Camera;
import mobile.arghub.planfactory.service.model.Controller;
import mobile.arghub.planfactory.service.model.Device;
import mobile.arghub.planfactory.service.model.Farm;
import mobile.arghub.planfactory.service.model.Season;
import mobile.arghub.planfactory.service.model.Sensor;
import mobile.arghub.planfactory.service.network.Config;
import mobile.arghub.planfactory.service.response_model.ChartDateResponse;
import mobile.arghub.planfactory.service.response_model.GetFarmResponse;
import mobile.arghub.planfactory.service.response_model.GetPlantResponse;
import mobile.arghub.planfactory.settings.SettingActivity;
import mobile.arghub.planfactory.utils.ControllerDefine;
import mobile.arghub.planfactory.utils.ControllerItem;
import mobile.arghub.planfactory.utils.SensorDefine;
import mobile.arghub.planfactory.utils.Utils;

public class MainActivity extends MainActivityHandleDevice implements PFService.Callback<GetFarmResponse> {

    private final int ADD_SEASON_KEY = 4587;

    PFService mService = PFService.getInstance();

    FarmNavigationAdapter mNavigationAdapter;

    CardView mSeasonInfoContainer;
    CardView mNoFarmContainer;
    TextView mSeasonPlantName;
    TextView mSeasonStartDate;
    TextView mSeasonPlantAge;
    ImageView mSeasonPlantImage;

    private int currentFarmIndex = -1;
    private String currentFarmID;

    ScheduledExecutorService updateScheduler;
    boolean windowIsInBackground = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupControlEvents();

        setupUpdatingService();
    }

    void setupUpdatingService() {
        updateScheduler = Executors.newSingleThreadScheduledExecutor();

        updateScheduler.scheduleAtFixedRate
                (new Runnable() {
                    public void run() {
                        if (windowIsInBackground == false) {
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mService.getFarm(MainActivity.this);
                                }
                            });
                        }
                    }
                }, 5, 5, TimeUnit.SECONDS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        updateScheduler.shutdown();
    }

    @Override
    protected void onPause() {
        super.onPause();

        windowIsInBackground = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        windowIsInBackground = false;

        showLoading();

        mCameraView.handleOnResume();

        mService.getPlants(new PFService.Callback<GetPlantResponse>() {
            @Override
            public void onSuccess(GetPlantResponse data) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateGeneralFarmList();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onResume();
                        //hideLoading();
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        closeMenu(false);

        if (requestCode == UPDATE_AVATAR_KEY) {
            updateAvatar();
        }

        if (requestCode == ADD_SEASON_KEY) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("success");
                if (result.equalsIgnoreCase("1")) {
                    updateGeneralFarmList();
                    String title = getString(R.string.app_name);
                    String mess = getString(R.string.new_season_message_success);
                    showDialog(title, mess);
                }
            }
        }
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();

        mNoFarmContainer = (CardView) findViewById(R.id.cv_no_farm_container);
        mSeasonInfoContainer = (CardView) findViewById(R.id.cv_season_info_container);
        mSeasonPlantName = (TextView) findViewById(R.id.tv_season_plant_name);
        mSeasonStartDate = (TextView) findViewById(R.id.tv_season_start_date);
        mSeasonPlantAge = (TextView) findViewById(R.id.tv_season_plant_age);
        mSeasonPlantImage = (ImageView) findViewById(R.id.iv_season_plant_image);
    }

    @Override
    void setupControlEvents() {
        super.setupControlEvents();

        mNavigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mNavigationAdapter.setSelectedItem(position);
                mNavigationAdapter.notifyDataSetChanged();
                updateInfoForFarm(position);
                closeMenu(true);
            }
        });
    }

    private void updateInfoForFarm(int position) {
        try {
            Farm selectedFarm = mNavigationAdapter.getItem(position);

            if (currentFarmIndex != position) {
                updateInfoForCamera(selectedFarm.camera);
            }

            currentFarmIndex = position;
            currentFarmID = selectedFarm.farm_id + "";

            mTitle.setText(selectedFarm.farm_name);

            updateInfoForSeason(selectedFarm.season);

            mCameraGroup.setVisibility(View.VISIBLE);

            if (selectedFarm.devices == null || selectedFarm.devices.size() == 0) {
                updateInfoForController(null);
                updateInfoForSensor(null);
                updateUIForControllerAndSensorContainer();
                updateErrorMessage(null, null);
                return;
            }

            List<Controller> updateControllerList = new ArrayList<>();
            List<Sensor> updateSensorList = new ArrayList<>();

            for (Device d : selectedFarm.devices) {
                if (d.controllers != null) {
                    updateControllerList.addAll(d.controllers);
                }

                if (d.sensors != null) {
                    for (Sensor s : d.sensors) {
                        s.deviceName = d.device_name;
                    }

                    updateSensorList.addAll(d.sensors);
                }
            }

            updateInfoForController(updateControllerList);
            updateInfoForSensor(updateSensorList);
            updateUIForControllerAndSensorContainer();
            updateErrorMessage(updateControllerList, updateSensorList);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            onNoFarms();
        }
    }

    private void updateInfoForCamera(List<Camera> cameras) {
        if (cameras != null && cameras.size() > 0) {
            mCameraContainer.setVisibility(View.VISIBLE);
            mCameraView.setLink(cameras.get(0).camera_stream_uri);
        }
        else {
            mCameraContainer.setVisibility(View.GONE);
        }
    }

    private void updateErrorMessage(List<Controller> controllerList, List<Sensor> sensorList) {
        if (controllerList == null && sensorList == null) {
            mErrorMessage.setVisibility(View.GONE);
            return;
        }

        List<String> errorControllers = new ArrayList<>();
        List<String> errorSensors = new ArrayList<>();
        if (controllerList != null) {
            for (Controller c : controllerList) {
                if (c.is_error) {
                    errorControllers.add(controllerUtils.getNameOfControllerByType(MainActivity.this, c.controller_type));
                }
            }
        }

        if (sensorList != null) {
            for (Sensor s : sensorList) {
                if (s.is_error) {
                    errorSensors.add(sensorUtils.getSensorNameByType(MainActivity.this, s.sensor_type));
                }
            }
        }

        String errorMessage = getString(R.string.error_message_first_part);
        Boolean isShowErrorMessage = false;
        if (errorControllers.size() > 0) {
            errorMessage += getString(R.string.error_message_controller_title);
            for (String c : errorControllers) {
                if (!c.isEmpty()) {
                    errorMessage += c + " ,";
                }
            }

            errorMessage = errorMessage.substring(0, errorMessage.length() - 1);
            isShowErrorMessage = true;
        }

        errorMessage += "\n";
        if (errorSensors.size() > 0) {
            errorMessage += getString(R.string.error_message_sensor_title);
            for (String s : errorSensors) {
                if (!s.isEmpty()) {
                    errorMessage += s + " ,";
                }
            }

            errorMessage = errorMessage.substring(0, errorMessage.length() - 1);
            isShowErrorMessage = true;
        }

        if (isShowErrorMessage) {
            mErrorMessage.setText(errorMessage);
            mErrorMessage.setVisibility(View.VISIBLE);
        }
        else {
            errorMessage = "";
            mErrorMessage.setVisibility(View.GONE);
        }

        if (!errorMessage.isEmpty()) {
            errorMessage += "\n";
        }

        Boolean showLeakedMessage = false;
        if (sensorList != null) {
            for (Sensor s : sensorList) {
                if (s.sensor_type == SensorDefine.WATER_LEAK_SENSOR) {
                    if (s.sensor_value == 1) {
                        showLeakedMessage = true;
                    }
                }
            }
        }

        if (showLeakedMessage) {
            errorMessage += getString(R.string.error_message_sensor_water_leaked);
            mErrorMessage.setText(errorMessage);
            mErrorMessage.setVisibility(View.VISIBLE);
        }
    }

    private void updateInfoForController(List<Controller> controllerList) {
        controllerUtils.prepareForUpdateData();
        if (controllerList == null) {
            controllerUtils.updateData();
            return;
        }

        controllerUtils.setValues(controllerList);
        controllerUtils.updateData();
    }

    private void updateInfoForSensor(List<Sensor> sensorList) {
        sensorUtils.prepareForUpdateData();
        if (sensorList == null) {
            sensorUtils.updateData();
            return;
        }

        sensorUtils.setValues(sensorList);
        sensorUtils.updateData();
    }

    private void updateInfoForSeason(Season season) {
        if (season == null) {
            mSeasonInfoContainer.setVisibility(View.GONE);
        }
        else {
            mSeasonInfoContainer.setVisibility(View.VISIBLE);
            String seasonText = mService.getPlantNameByID(season.plant_id);
            mSeasonPlantName.setText(seasonText);

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String dateString = formatter.format(new Date(season.season_state_date));

            mSeasonStartDate.setText(getString(R.string.season_info_start_date) + " " + dateString);
            mSeasonPlantAge.setText(getString(R.string.season_info_plant_age) + " " + season.season_age_of_plant + " " + getString(R.string.day));
            Picasso.with(MainActivity.this).load(mService.getPlanImageByID(season.plant_id)).transform(new Utils.CircleTransform()).into(mSeasonPlantImage);
        }
    }

    private void updateGeneralFarmList() {
        mService.getFarm(new PFService.Callback<GetFarmResponse>() {
            @Override
            public void onSuccess(final GetFarmResponse data) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        if (data == null) {
                            onNoFarms();
                            return;
                        }

                        mNoFarmContainer.setVisibility(View.GONE);
                        mNewSeasonMenuItem.setVisible(true);
                        mSettingsMenuItem.setVisible(true);
                        mNoFarmLable.setVisibility(View.GONE);
                        mNavigationList.setVisibility(View.VISIBLE);
                        if (mNavigationAdapter == null) {
                            mNavigationAdapter = new FarmNavigationAdapter(MainActivity.this, data.farms);
                            mNavigationAdapter.setSelectedItem(0);
                            updateInfoForFarm(0);
                            mNavigationList.setAdapter(mNavigationAdapter);
                            return;
                        }

                        mNavigationAdapter.clear();
                        mNavigationAdapter.addAll(data.farms);
                        mNavigationAdapter.notifyDataSetChanged();
                        updateInfoForFarm(currentFarmIndex);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                hideLoading();
                //TODO
            }
        });
    }

    private void onNoFarms() {
        mNoFarmContainer.setVisibility(View.VISIBLE);
        mNavigationList.setVisibility(View.GONE);
        mNoFarmLable.setVisibility(View.VISIBLE);
        mNewSeasonMenuItem.setVisible(false);
        mSettingsMenuItem.setVisible(false);
        mCameraGroup.setVisibility(View.GONE);
    }

    @Override
    protected void addNewSeason() {
        super.addNewSeason();

        Intent intent = new Intent(MainActivity.this, NewSeasonActivity.class);
        intent.putExtra("farmID", currentFarmID);
        startActivityForResult(intent, ADD_SEASON_KEY);
    }

    @Override
    protected void gotoSetting() {
        super.gotoSetting();

        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
        intent.putExtra("farmID", currentFarmID);
        startActivity(intent);
    }

    @Override
    protected void requestLogout() {
        super.requestLogout();

        final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.dialog_logout_message))
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        doLogout();
                        dialog.dismiss();
                    }
                }).create();

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }


    void doLogout() {
        mService.currentUser = null;
        mService.currentAccessToken = null;
        Utils.savePreference(MainActivity.this, Config.USER_KEY, "");
        Utils.savePreference(MainActivity.this, Config.AVATAR_KEY, "");
        Utils.savePreference(MainActivity.this, Config.USER_TOKEN_KEY, "");

        Intent intent = new Intent(MainActivity.this, PhoneRegistrationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSuccess(final GetFarmResponse data) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (data == null) {
                    onNoFarms();
                    return;
                }

                mNoFarmContainer.setVisibility(View.GONE);
                mNewSeasonMenuItem.setVisible(true);
                mSettingsMenuItem.setVisible(true);
                mNoFarmLable.setVisibility(View.GONE);
                mNavigationList.setVisibility(View.VISIBLE);
                if (mNavigationAdapter == null) {
                    mNavigationAdapter = new FarmNavigationAdapter(MainActivity.this, data.farms);
                    mNavigationAdapter.setSelectedItem(0);
                    updateInfoForFarm(0);
                    mNavigationList.setAdapter(mNavigationAdapter);
                    return;
                }

                mNavigationAdapter.clear();
                mNavigationAdapter.addAll(data.farms);
                mNavigationAdapter.notifyDataSetChanged();
                updateInfoForFarm(currentFarmIndex);
            }
        });
    }

    @Override
    public void onFail(String error) {

    }

    @Override
    protected void getSensorDataWithLiquidValue(int diary) {
        super.getSensorDataWithLiquidValue(diary);

        mService.getSensorChartDataWithLiquidValue(currentFarmID, chartCurrentSensorSelected.sensor_id + "", diary + "", new PFService.Callback<ChartDateResponse>() {
            @Override
            public void onSuccess(final ChartDateResponse data) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDateForChartWithLiquidValue(data);
                        chartView.setVisibility(View.VISIBLE);
                        chartLoadingBar.setVisibility(View.INVISIBLE);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    }
}
