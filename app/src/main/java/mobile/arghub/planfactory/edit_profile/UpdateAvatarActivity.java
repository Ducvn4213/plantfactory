package mobile.arghub.planfactory.edit_profile;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.network.Config;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;

interface UpdateAvatarInterface {
    void bindingControl();
    void setupControlEvents();
    void initData();
    void requestChangeAvatar();
    void updateAvatar();
    void gotoCompleted(String imageDecodedString);
}

public class UpdateAvatarActivity extends BaseActivity implements UpdateAvatarInterface {

    private final int PICK_IMAGE_REQUEST_CODE = 9999;
    private final int TAKE_IMAGE_REQUEST_CODE = 8888;

    ImageButton mAvatar;
    Button mContinue;

    private Bitmap mNewBitmapAvatar;
    private Bitmap mNewDisplayAvatar;

    PFService mService = PFService.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_avatar_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControl() {
        mAvatar = (ImageButton) findViewById(R.id.ib_avatar);
        mContinue = (Button) findViewById(R.id.btn_continue);
    }

    @Override
    public void setupControlEvents() {
        mAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestChangeAvatar();
            }
        });

        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAvatar();
            }
        });
    }

    @Override
    public void initData() {
        Bitmap avatar = mService.getCurrentAvatarBitmap(UpdateAvatarActivity.this);
        if (avatar != null) {
            mAvatar.setImageBitmap(Utils.getCircleBitmap(avatar));
            return;
        }

        Picasso.with(UpdateAvatarActivity.this).load(mService.currentUser.user_avatar).into(target);
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mAvatar.setImageBitmap(Utils.getCircleBitmap(bitmap));
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    @Override
    public void requestChangeAvatar() {
        final Dialog actionDialog = new Dialog(UpdateAvatarActivity.this);
        actionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        actionDialog.setCancelable(true);
        actionDialog.setContentView(R.layout.layout_change_avatar_action);

        TextView camera = (TextView) actionDialog.findViewById(R.id.tv_take_from_camera);
        TextView gallery = (TextView) actionDialog.findViewById(R.id.tv_get_from_gallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                checkCameraPermission();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(UpdateAvatarActivity.this, Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE, null);
            }
        });

        actionDialog.show();
    }

    void checkCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(UpdateAvatarActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            gotoTakeImageFromCamera();
        }
        else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(UpdateAvatarActivity.this,
                    perArr,
                    Config.PERMISSIONS_CAMERA_REQUEST);
        }
    }

    void gotoTakeImageFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePicture, TAKE_IMAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Config.PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    gotoTakeImageFromCamera();
                    return;
                }
                else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                    return;
                }
            }
            else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                return;
            }
        }
    }

    @Override
    public void updateAvatar() {
        if (mNewBitmapAvatar == null) {
            gotoCompleted(null);
            return;
        }

        showLoading();
        mService.updateAvatar(mNewBitmapAvatar, new PFService.Callback<String>() {
            @Override
            public void onSuccess(final String data) {
                UpdateAvatarActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        gotoCompleted(data);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                UpdateAvatarActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.phone_registration_avatar_error));
                    }
                });
            }
        });
    }

    @Override
    public void gotoCompleted(String imageDecodedString) {
        if (imageDecodedString != null) {
            Utils.savePreference(UpdateAvatarActivity.this, Config.AVATAR_KEY, imageDecodedString);
            mService.removeOldAvatar(UpdateAvatarActivity.this);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Uri filePath = data.getData();
                mNewBitmapAvatar = MediaStore.Images.Media.getBitmap(UpdateAvatarActivity.this.getContentResolver(), filePath);
                mNewDisplayAvatar = Utils.getCircleBitmap(mNewBitmapAvatar);
                mNewBitmapAvatar = MediaStore.Images.Media.getBitmap(UpdateAvatarActivity.this.getContentResolver(), filePath);
                mAvatar.setImageBitmap(mNewDisplayAvatar);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                UpdateAvatarActivity.this.showErrorDialog(getString(R.string.dialog_error_cant_set_avatar));
            }
        }

        if (requestCode == TAKE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                mNewBitmapAvatar = (Bitmap) data.getExtras().get("data");
                mNewDisplayAvatar = Utils.getCircleBitmap(mNewBitmapAvatar);
                mNewBitmapAvatar = (Bitmap) data.getExtras().get("data");
                mAvatar.setImageBitmap(mNewDisplayAvatar);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                UpdateAvatarActivity.this.showErrorDialog(getString(R.string.dialog_error_cant_set_avatar));
            }
        }
    }
}
