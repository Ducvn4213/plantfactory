package mobile.arghub.planfactory.registration;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import mobile.arghub.planfactory.MainActivity;
import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;


interface PhoneRegistrationCompletedInterface {
    void bindingControl();
    void setupControlEvents();
    void initData();
    void gotoMain();
}

public class PhoneRegistrationCompletedActivity extends BaseActivity implements PhoneRegistrationCompletedInterface {

    private final int PICK_IMAGE_REQUEST_CODE = 9999;

    ImageButton mCompletedImage;
    Button mContinue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_completed_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControl() {
        mCompletedImage = (ImageButton) findViewById(R.id.ib_completed_image);
        mContinue = (Button) findViewById(R.id.btn_continue);
    }

    @Override
    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMain();
            }
        });
    }

    @Override
    public void initData() {
        Bitmap defaultAvatar = BitmapFactory.decodeResource(PhoneRegistrationCompletedActivity.this.getResources(), R.drawable.profile);
        mCompletedImage.setImageBitmap(Utils.getCircleBitmap(defaultAvatar));
    }

    @Override
    public void gotoMain() {
        Intent intent = new Intent(PhoneRegistrationCompletedActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
