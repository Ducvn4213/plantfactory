package mobile.arghub.planfactory.registration;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.User;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;

interface PhoneRegistrationPersonalInformationInterface {
    void bindingControl();
    void setupControlEvents();
    void initData();
    void applySetting();
    void gotoSetAvatar();
}

public class PhoneRegistrationPersonalInformationActivity extends BaseActivity implements PhoneRegistrationPersonalInformationInterface, DatePickerDialog.OnDateSetListener {

    private static final String DATE_PICKER_START_DIALOG_TAG = "DATE_PICKER_START_DIALOG_TAG";

    AppCompatSpinner mGender;
    Button mBirth;
    Button mContinue;
    EditText mFirstName;
    EditText mLastName;
    EditText mEmail;

    Calendar myCalendar = Calendar.getInstance();
    long mBirthDayByTM;

    PFService mService = PFService.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_personal_information_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControl() {
        mGender = (AppCompatSpinner) findViewById(R.id.spn_gender);
        mBirth = (Button) findViewById(R.id.btn_birth);
        mContinue = (Button) findViewById(R.id.btn_continue);
        mFirstName = (EditText) findViewById(R.id.et_first_name);
        mLastName = (EditText) findViewById(R.id.et_last_name);
        mEmail = (EditText) findViewById(R.id.et_email);
    }

    @Override
    public void setupControlEvents() {
        mBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(PhoneRegistrationPersonalInformationActivity.this, PhoneRegistrationPersonalInformationActivity.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mContinue.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                applySetting();
            }
        });
    }

    @Override
    public void initData() {
        List<String> genderList = new ArrayList<>();
        String[] items = getResources().getStringArray(R.array.array_gender);
        if (genderList.size() == 0) {
            Collections.addAll(genderList, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item,
                R.id.tv_item_text, genderList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mGender.setAdapter(pm_adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void applySetting() {
        String firstName = mFirstName.getText().toString();
        String lastName = mLastName.getText().toString();
        String email = mEmail.getText().toString();
        String birthDay = mBirth.getText().toString();
        int indexGender = mGender.getSelectedItemPosition();

        if (firstName.isEmpty()) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_first_name_empty));
            return;
        }

        if (lastName.isEmpty()) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_last_name_empty));
            return;
        }

        if (!Utils.isValidEmail(email)) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_email_empty));
            return;
        }

        if (birthDay.isEmpty()) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_birth_empty));
            return;
        }

        if (indexGender == 0) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_gender_empty));
            return;
        }

        showLoading();
        mService.updateProfile(PhoneRegistrationPersonalInformationActivity.this, firstName, lastName, email, this.mBirthDayByTM + "", indexGender == 1 ? "1" : "0", new PFService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                PhoneRegistrationPersonalInformationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        gotoSetAvatar();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                PhoneRegistrationPersonalInformationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.phone_registration_personal_information_error_update_fail));
                    }
                });
            }
        });
    }

    @Override
    public void gotoSetAvatar() {
        Intent intent = new Intent(PhoneRegistrationPersonalInformationActivity.this, PhoneRegistrationAvatarActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        this.mBirthDayByTM = myCalendar.getTimeInMillis();
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        mBirth.setText(sdf.format(myCalendar.getTime()));
    }
}
