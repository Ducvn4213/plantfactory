package mobile.arghub.planfactory.registration;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.arghub.planfactory.MainActivity;
import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.User;
import mobile.arghub.planfactory.service.network.Config;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;

interface PhoneRegistrationConfirmInterface {
    void bindingControl();
    void setupControlEvents();
    void initData();
    void checkCode();
    void gotoPersonalInformation();
    void gotoDashboard();
}

public class PhoneRegistrationConfirmActivity extends BaseActivity implements PhoneRegistrationConfirmInterface {

    Button mContinue;
    TextView mPhone;
    EditText mCode;

    PFService mService = PFService.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_confirm_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControl() {
        mContinue = (Button) findViewById(R.id.btn_continue);
        mPhone = (TextView) findViewById(R.id.tv_phone);
        mCode = (EditText) findViewById(R.id.et_code);
    }

    @Override
    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                checkCode();
            }
        });
    }

    @Override
    public void initData() {
        String phone = getIntent().getStringExtra("phone");
        mPhone.setText(phone);
    }

    @Override
    public void checkCode() {
        String code = mCode.getText().toString();
        if (code.isEmpty()) {
            String errorMessage = getString(R.string.phone_registration_error_empty_code);
            showErrorDialog(errorMessage);
            return;
        }

        showLoading();
        getCurrentLocationAndContinue();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void afterGotCurrentLocation(double lat, double lon) {
        super.afterGotCurrentLocation(lat, lon);

        String code = mCode.getText().toString();
        String phone = mPhone.getText().toString();
        mService.verifyCode(PhoneRegistrationConfirmActivity.this, phone, code, lat + "", lon + "", new PFService.Callback<String>() {
            @Override
            public void onSuccess(final String data) {
                PhoneRegistrationConfirmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        if (data.equalsIgnoreCase("1")) {
                            gotoPersonalInformation();
                        }
                        else {
                            gotoDashboard();
                        }
                    }
                });
            }

            @Override
            public void onFail(final String error) {
                PhoneRegistrationConfirmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(error);
                    }
                });
            }
        });
    }

    @Override
    protected void afterGetCurrentLocationFail() {
        super.afterGetCurrentLocationFail();
        hideLoading();
    }

    @Override
    public void gotoPersonalInformation() {
        Intent intent = new Intent(PhoneRegistrationConfirmActivity.this, PhoneRegistrationPersonalInformationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void gotoDashboard() {
        Intent intent = new Intent(PhoneRegistrationConfirmActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
