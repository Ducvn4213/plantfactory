package mobile.arghub.planfactory.registration;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.Nation;
import mobile.arghub.planfactory.service.response_model.NationResponse;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;

interface PhoneRegistrationInterface {
    void bindingControl();
    void setupControlEvents();
    void initData();
    void sendCode();
    void gotoConfirmScreen();
    String correctPhoneNumber();
}

public class PhoneRegistrationActivity extends BaseActivity implements PhoneRegistrationInterface {

    Button mContinue;
    EditText mPhone;
    AppCompatSpinner mZone;

    PFService mService = PFService.getInstance();

    List<Nation> nationList;
    List<String> nationListValue;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControl() {
        mContinue = (Button) findViewById(R.id.btn_continue);
        mPhone = (EditText) findViewById(R.id.et_phone);
        mZone = (AppCompatSpinner) findViewById(R.id.spn_zone);
    }

    @Override
    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                PhoneRegistrationActivity.this.sendCode();
            }
        });
    }

    @Override
    public void initData() {
        showLoading();
        mService.getNations(new PFService.Callback<NationResponse>() {
            @Override
            public void onSuccess(final NationResponse data) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        setupZoneData(data.data);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.phone_registration_error_from_server_get_nations));
                        mContinue.setEnabled(false);
                    }
                });
            }
        });
    }

    void setupZoneData(List<Nation> data) {
        nationList = data;

        nationListValue = new ArrayList<>();
        for (Nation nation : data) {
            nationListValue.add("(" + nation.phone_code + ") " + nation.nation_name);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item,
                R.id.tv_item_text, nationListValue);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mZone.setAdapter(pm_adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void sendCode() {
        String phone = mPhone.getText().toString();
        if (phone.isEmpty()) {
            String errorString = getString(R.string.phone_registration_error_empty_phone);
            showErrorDialog(errorString);
            return;
        }

        phone = correctPhoneNumber();

        showLoading();
        mService.getPassCode(phone, new PFService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        gotoConfirmScreen();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        String errorString = getString(R.string.phone_registration_error_from_server);
                        showErrorDialog(errorString);
                    }
                });
            }
        });
    }

    @Override
    public void gotoConfirmScreen() {
        Intent intent = new Intent(PhoneRegistrationActivity.this, PhoneRegistrationConfirmActivity.class);
        intent.putExtra("phone", correctPhoneNumber());
        startActivity(intent);
        finish();
    }

    @Override
    public String correctPhoneNumber() {
        String phone = mPhone.getText().toString();
        phone = phone.trim();

        if (phone.startsWith("0")) {
            phone = phone.substring(1);
        }

        int zoneIndex = mZone.getSelectedItemPosition();

        phone = nationList.get(zoneIndex).phone_code + phone;

        return phone;
    }
}
