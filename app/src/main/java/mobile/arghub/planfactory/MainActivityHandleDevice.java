package mobile.arghub.planfactory;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.Sensor;
import mobile.arghub.planfactory.service.response_model.ChartDateResponse;
import mobile.arghub.planfactory.utils.CameraItem;
import mobile.arghub.planfactory.utils.ControllerItem;
import mobile.arghub.planfactory.utils.ControllerUtils;
import mobile.arghub.planfactory.utils.PercentItem;
import mobile.arghub.planfactory.utils.SensorDefine;
import mobile.arghub.planfactory.utils.SensorItem;
import mobile.arghub.planfactory.utils.SensorUtils;
import mobile.arghub.planfactory.utils.Utils;

public class MainActivityHandleDevice extends MainActivityBase implements SensorItem.SensorItemOnClickListener{
    protected ControllerItem mWaterPumpController;
    //protected ControllerItem mOxygenPumpController;
    protected ControllerItem mWashingModeController;

    protected LinearLayout mCameraContainer;
    protected CameraItem mCameraView;

    protected SensorItem mLightSensor;
    protected SensorItem mAirTempSensor;
    protected SensorItem mAirHumiditySensor;
    protected SensorItem mSoilTempSensor;
    protected SensorItem mSoilHumiditySensor;
    protected SensorItem mSoilECSensor;
    protected SensorItem mSoilPHSensor;
    protected SensorItem mWaterTempSensor;
    protected SensorItem mWaterECSensor;
    protected SensorItem mWaterPHSensor;
    protected SensorItem mWaterORPSensor;
    protected SensorItem mBatterySensor;
    protected SensorItem mCO2Sensor;
    protected SensorItem mWaterLevelSensor;
    protected SensorItem mWaterLeakSensor;
    protected SensorItem mErrorSensor;

    protected ControllerItem mLampController;
    protected ControllerItem mMistingPumpController;
    protected ControllerItem mFanController;
    //protected ControllerItem mAirConditionerController;
    //protected ControllerItem mCO2Controller;
    protected ControllerItem mDosingPumpController;

    protected CardView mDevicesGroup;
    protected CardView mOutsideControllerGroup;
    protected CardView mSensorGroup;
    protected CardView mCameraGroup;

    protected TextView mErrorMessage;

    protected ControllerUtils controllerUtils = ControllerUtils.getInstance();
    protected SensorUtils sensorUtils = SensorUtils.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindingControls();
        initControllerSensorUtils();
    }

    protected void bindingControls() {
        mCameraContainer = (LinearLayout) findViewById(R.id.ll_camera_container);
        mCameraView = (CameraItem) findViewById(R.id.cai_camera);

        mWaterPumpController = (ControllerItem) findViewById(R.id.ci_water_pump);
        //mOxygenPumpController = (ControllerItem) findViewById(R.id.ci_oxygen_pump);
        mWashingModeController = (ControllerItem) findViewById(R.id.ci_washing_mode);

        mLightSensor = (SensorItem) findViewById(R.id.si_light);
        mAirTempSensor = (SensorItem) findViewById(R.id.si_air_temp);
        mAirHumiditySensor = (SensorItem) findViewById(R.id.si_air_humidity);
        mSoilTempSensor = (SensorItem) findViewById(R.id.si_soil_temp);
        mSoilHumiditySensor = (SensorItem) findViewById(R.id.si_soil_humidity);
        mSoilECSensor = (SensorItem) findViewById(R.id.si_soil_ec);
        mSoilPHSensor = (SensorItem) findViewById(R.id.si_soil_ph);
        mWaterTempSensor = (SensorItem) findViewById(R.id.si_water_temp);
        mWaterECSensor = (SensorItem) findViewById(R.id.si_water_ec);
        mWaterPHSensor = (SensorItem) findViewById(R.id.si_water_ph);
        mWaterORPSensor = (SensorItem) findViewById(R.id.si_water_orp);
        mBatterySensor = (SensorItem) findViewById(R.id.si_battery);
        mCO2Sensor = (SensorItem) findViewById(R.id.si_co2);
        mWaterLevelSensor = (SensorItem) findViewById(R.id.si_water_level);
        mWaterLeakSensor = (SensorItem) findViewById(R.id.si_water_leak);
        mErrorSensor = (SensorItem) findViewById(R.id.si_error);

        mLampController = (ControllerItem) findViewById(R.id.ci_lamp);
        mMistingPumpController = (ControllerItem) findViewById(R.id.ci_misting_pump);
        mFanController = (ControllerItem) findViewById(R.id.ci_fan);
        //mAirConditionerController = (ControllerItem) findViewById(R.id.ci_air_conditioner);
        //mCO2Controller = (ControllerItem) findViewById(R.id.ci_co2);
        mDosingPumpController = (ControllerItem) findViewById(R.id.ci_dosing_pump);

        mDevicesGroup = (CardView) findViewById(R.id.cv_devices_container);
        mOutsideControllerGroup = (CardView) findViewById(R.id.cv_outside_controllers_container);
        mSensorGroup = (CardView) findViewById(R.id.cv_sensors_container);
        mCameraGroup = (CardView) findViewById(R.id.cv_camera_container);

        mErrorMessage = (TextView) findViewById(R.id.tv_error_message);

        mLightSensor.setOnSensorItemClick(this);
        mAirTempSensor.setOnSensorItemClick(this);
        mAirHumiditySensor.setOnSensorItemClick(this);
        mSoilTempSensor.setOnSensorItemClick(this);
        mSoilHumiditySensor.setOnSensorItemClick(this);
        mSoilECSensor.setOnSensorItemClick(this);
        mSoilPHSensor.setOnSensorItemClick(this);
        mWaterTempSensor.setOnSensorItemClick(this);
        mWaterECSensor.setOnSensorItemClick(this);
        mWaterPHSensor.setOnSensorItemClick(this);
        mWaterORPSensor.setOnSensorItemClick(this);
        mBatterySensor.setOnSensorItemClick(this);
        mCO2Sensor.setOnSensorItemClick(this);
        mWaterLevelSensor.setOnSensorItemClick(this);
        mWaterLeakSensor.setOnSensorItemClick(this);
        mErrorSensor.setOnSensorItemClick(this);
    }

    protected void updateUIForControllerAndSensorContainer() {
        updateUIForControllerContainer(mDevicesGroup);
        updateUIForControllerContainer(mOutsideControllerGroup);
        updateUIForSensorContainer(mSensorGroup);
    }

    private void updateUIForControllerContainer(CardView target) {
        boolean needToGone = true;
        int count = target.getChildCount();
        if (count > 0) {
            ViewGroup flowLayout = (ViewGroup) target.getChildAt(0);
            count = flowLayout.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = flowLayout.getChildAt(i);
                if (child.getVisibility() == View.VISIBLE) {
                    needToGone = false;
                    break;
                }
            }
        }

        target.setVisibility(needToGone ? View.GONE : View.VISIBLE);
    }

    private void updateUIForSensorContainer(CardView target) {
        boolean needToGone = true;
        int count = target.getChildCount();
        if (count > 0) {
            ViewGroup flowLayout = (ViewGroup) target.getChildAt(0);
            count = flowLayout.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = flowLayout.getChildAt(i);
                if (child.getVisibility() == View.VISIBLE) {
                    needToGone = false;
                    break;
                }
            }
        }

        target.setVisibility(needToGone ? View.GONE : View.VISIBLE);
    }

    void initControllerSensorUtils() {
        //controllerUtils.setOxygenPumpController(mOxygenPumpController);
        controllerUtils.setWashingModeController(mWashingModeController);
        controllerUtils.setWaterPumpController(mWaterPumpController);

        //controllerUtils.setAirConditionerController(mAirConditionerController);
        //controllerUtils.setCO2Controller(mCO2Controller);
        controllerUtils.setDosingPumpController(mDosingPumpController);
        controllerUtils.setFanController(mFanController);
        controllerUtils.setLampController(mLampController);
        controllerUtils.setMistingPumpController(mMistingPumpController);

        sensorUtils.setAirHumiditySensor(mAirHumiditySensor);
        sensorUtils.setAirTempSensor(mAirTempSensor);
        sensorUtils.setBatterySensor(mBatterySensor);
        sensorUtils.setcO2Sensor(mCO2Sensor);
        sensorUtils.setErrorSensor(mErrorSensor);
        sensorUtils.setLightSensor(mLightSensor);
        sensorUtils.setSoilECSensor(mSoilECSensor);
        sensorUtils.setSoilHumiditySensor(mSoilHumiditySensor);
        sensorUtils.setSoilPHSensor(mSoilPHSensor);
        sensorUtils.setSoilTempSensor(mSoilTempSensor);
        sensorUtils.setWaterECSensor(mWaterECSensor);
        sensorUtils.setWaterLeakSensor(mWaterLeakSensor);
        sensorUtils.setWaterLevelSensor(mWaterLevelSensor);
        sensorUtils.setWaterORPSensor(mWaterORPSensor);
        sensorUtils.setWaterTempSensor(mWaterTempSensor);
        sensorUtils.setWaterPHSensor(mWaterPHSensor);
    }

    @Override
    public void onSensorClick(Sensor item) {
        this.showChartForSensor(item);
    }

    Dialog chartDialog = null;
    Button chartButtonUTM;
    Button chartButtonHourly;
    Button chartButtonDaily;
    Button chartButtonWeekly;
    Button chartButtonMonthly;
    LineChart chartView;
    ProgressBar chartLoadingBar;
    TextView chartSensorName;
    TextView chartSensorCurrentValue;
    TextView chartSensorPin;
    LinearLayout chartPercentContainer;
    PercentItem chartPercentA;
    PercentItem chartPercentB;
    PercentItem chartPercentC;
    PercentItem chartPercentD;

    Sensor chartCurrentSensorSelected = null;
    int chartCurrentDairySelected = Utils.DiaryType_UTM;

    void showChartForSensor(Sensor item) {
        if (chartDialog == null) {
            initChartDialog();
            showChartForSensor(item);
        }

        configPercentContainer();
        if (item.sensor_type == SensorDefine.WATER_EC_SENSOR) {
            chartPercentContainer.setVisibility(View.VISIBLE);
            chartPercentA.setVisibility(View.VISIBLE);
            chartPercentB.setVisibility(View.VISIBLE);
            chartPercentC.setVisibility(View.VISIBLE);
            chartPercentD.setVisibility(View.INVISIBLE);
        }
        else if (item.sensor_type == SensorDefine.WATER_PH_SENSOR) {
            chartPercentContainer.setVisibility(View.VISIBLE);
            chartPercentD.setVisibility(View.VISIBLE);
        }

        setCurrentSensorSelected(item);
        setChartDialogTo(Utils.DiaryType_UTM);
        chartDialog.show();

        updateUIBatteryLevel(item);
    }

    void initChartDialog() {
        chartDialog = new Dialog(MainActivityHandleDevice.this);
        chartDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        chartDialog.setCancelable(true);
        chartDialog.setContentView(R.layout.layout_chart_view);

        chartButtonUTM = (Button) chartDialog.findViewById(R.id.btn_chart_utm);
        chartButtonHourly = (Button) chartDialog.findViewById(R.id.btn_chart_hourly);
        chartButtonDaily = (Button) chartDialog.findViewById(R.id.btn_chart_daily);
        chartButtonWeekly = (Button) chartDialog.findViewById(R.id.btn_chart_weekly);
        chartButtonMonthly = (Button) chartDialog.findViewById(R.id.btn_chart_monthly);

        chartView = (LineChart) chartDialog.findViewById(R.id.lc_chart);
        chartLoadingBar = (ProgressBar) chartDialog.findViewById(R.id.pb_chart_loading);

        chartSensorName = (TextView) chartDialog.findViewById(R.id.tv_sensor_name);
        chartSensorCurrentValue = (TextView) chartDialog.findViewById(R.id.tv_sensor_current_value);
        chartSensorPin = (TextView) chartDialog.findViewById(R.id.tv_pin);

        chartPercentContainer = (LinearLayout) chartDialog.findViewById(R.id.ll_percent_zone_container);
        chartPercentA = (PercentItem) chartDialog.findViewById(R.id.pi_percent_item_a);
        chartPercentB = (PercentItem) chartDialog.findViewById(R.id.pi_percent_item_b);
        chartPercentC = (PercentItem) chartDialog.findViewById(R.id.pi_percent_item_c);
        chartPercentD = (PercentItem) chartDialog.findViewById(R.id.pi_percent_item_d);

        configChartView();
        configPercentContainer();

        setupHandleControlEventForChart(chartButtonUTM, Utils.DiaryType_UTM);
        setupHandleControlEventForChart(chartButtonHourly, Utils.DiaryType_Hourly);
        setupHandleControlEventForChart(chartButtonDaily, Utils.DiaryType_Daily);
        setupHandleControlEventForChart(chartButtonWeekly, Utils.DiaryType_Weekly);
        setupHandleControlEventForChart(chartButtonMonthly, Utils.DiaryType_Monthly);
    }

    void configPercentContainer() {
        chartPercentContainer.setVisibility(View.GONE);
        chartPercentA.setVisibility(View.GONE);
        chartPercentB.setVisibility(View.GONE);
        chartPercentC.setVisibility(View.GONE);
        chartPercentD.setVisibility(View.GONE);
    }

    void configChartView() {
        chartView.setDrawGridBackground(false);

        chartView.getDescription().setEnabled(false);

        chartView.setTouchEnabled(false);

        chartView.setDragEnabled(false);
        chartView.setScaleEnabled(false);

        chartView.setPinchZoom(false);

        YAxis leftAxis = chartView.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setAxisMaximum(200f);
        leftAxis.setAxisMinimum(-50f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        leftAxis.setDrawLimitLinesBehindData(true);

        chartView.getAxisRight().setEnabled(false);
        chartView.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

//        XAxis xAxis = chartView.getXAxis();
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return "H";
//            }
//        });

        Legend l = chartView.getLegend();

        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.WHITE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
    }

    void setupHandleControlEventForChart(Button target, final int requireIndex) {
        target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chartCurrentDairySelected != requireIndex) {
                    setChartDialogTo(requireIndex);
                }
            }
        });
    }

    void setCurrentSensorSelected(Sensor item) {
        this.chartCurrentSensorSelected = item;
        this.chartSensorName.setText(sensorUtils.getSensorNameByType(MainActivityHandleDevice.this, item.sensor_type));
    }

    void setChartDialogTo(int diary) {
        setChartSelectionButtonTo(diary);
        chartCurrentDairySelected = diary;
        chartView.setVisibility(View.INVISIBLE);
        chartLoadingBar.setVisibility(View.VISIBLE);
        if (chartCurrentSensorSelected.sensor_type == SensorDefine.WATER_PH_SENSOR) {
            getSensorChartDataForPH(diary);
        }
        else if (chartCurrentSensorSelected.sensor_type == SensorDefine.WATER_EC_SENSOR) {
            getSensorChartDataForEC(diary);
        }
        else {
            getSensorChartData(diary);
        }
    }

    void getSensorChartDataForEC(int diary) {
        getSensorDataWithLiquidValue(diary);
    }

    void getSensorChartDataForPH(int diary) {
        getSensorDataWithLiquidValue(diary);
    }

    protected void getSensorDataWithLiquidValue(int diary) {
        //TODO
    }

    void getSensorChartData(int diary) {
        mService.getSensorChartData(chartCurrentSensorSelected.sensor_id + "", diary + "", new PFService.Callback<ChartDateResponse>() {
            @Override
            public void onSuccess(final ChartDateResponse data) {
                MainActivityHandleDevice.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateDateForChart(data);
                        chartView.setVisibility(View.VISIBLE);
                        chartLoadingBar.setVisibility(View.INVISIBLE);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    }

    void updateDateForChart(ChartDateResponse _data) {
        if (_data == null || _data.data == null) {
            return;
        }

        ArrayList<Entry> values = new ArrayList<Entry>();

        int size = _data.data.size();
        float theIndex = 0;
        for (int i = (size - 1); i >= 0; i--) {
            Float v = new Float(_data.data.get(i).getValue());
            values.add(new Entry((float) theIndex, v));
            theIndex++;
        }

        if (size == 0) {
            chartSensorCurrentValue.setText("--");
        }
        else {
            chartSensorCurrentValue.setText(_data.data.get(size - 1).getValue() + "");
        }

        LineDataSet set1;
        if (chartView.getData() != null && chartView.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chartView.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chartView.getData().notifyDataChanged();
            chartView.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "DataSet 1");

            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setColor(ColorTemplate.getHoloBlue());
            set1.setCircleColor(ColorTemplate.getHoloBlue());
            set1.setLineWidth(2f);
            set1.setCircleRadius(3f);
            set1.setFillAlpha(65);
            set1.setFillColor(ColorTemplate.getHoloBlue());
            set1.setHighLightColor(Color.rgb(244, 117, 117));
            set1.setDrawCircleHole(false);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setFillColor(ColorTemplate.getHoloBlue());

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);

            chartView.setData(data);
        }
    }

    void updateDateForChartWithLiquidValue(ChartDateResponse _data) {
        if (_data == null) {
            return;
        }
        updateDateForChart(_data);

        if (chartCurrentSensorSelected.sensor_type == SensorDefine.WATER_EC_SENSOR) {
            chartPercentA.setValue(_data.factory_id, _data.percent_liquid_a);
            chartPercentB.setValue(_data.factory_id, _data.percent_liquid_b);
            chartPercentC.setValue(_data.factory_id, _data.percent_liquid_c);
        }
        else if (chartCurrentSensorSelected.sensor_type == SensorDefine.WATER_PH_SENSOR){
            chartPercentD.setValue(_data.factory_id, _data.percent_liquid_d);
        }
    }

    void setChartSelectionButtonTo(int type) {
        chartButtonUTM.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top_unactive));
        chartButtonHourly.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top_unactive));
        chartButtonDaily.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top_unactive));
        chartButtonWeekly.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top_unactive));
        chartButtonMonthly.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top_unactive));

        switch (type) {
            case Utils.DiaryType_UTM:
                chartButtonUTM.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top));
                break;
            case Utils.DiaryType_Hourly:
                chartButtonHourly.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top));
                break;
            case Utils.DiaryType_Daily:
                chartButtonDaily.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top));
                break;
            case Utils.DiaryType_Weekly:
                chartButtonWeekly.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top));
                break;
            case Utils.DiaryType_Monthly:
                chartButtonMonthly.setBackground(ContextCompat.getDrawable(MainActivityHandleDevice.this, R.drawable.button_rounder_top));
                break;
            default: break;

        }
    }

    protected void updateUIBatteryLevel(Sensor sensor) {
        if (SensorUtils.needToShowPIN(sensor)) {
            chartSensorPin.setVisibility(View.VISIBLE);
            if (chartDialog != null && chartDialog.isShowing()) {
                chartSensorPin.setText(sensorUtils.getBatteryLevel(sensor.deviceName));
            }
        }
        else {
            chartSensorPin.setVisibility(View.INVISIBLE);
        }
    }
}
