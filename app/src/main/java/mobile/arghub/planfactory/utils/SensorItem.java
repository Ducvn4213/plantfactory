package mobile.arghub.planfactory.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Date;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.model.Sensor;

public class SensorItem extends LinearLayout {

    public interface SensorItemOnClickListener {
        void onSensorClick(Sensor item);
    }

    private TextView mName;
    private TextView mIcon;
    private TextView mValue;
    private TextView mUpdatedTime;
    private TextView mError;
    Typeface mIconFont;
    Sensor mItem;

    private SensorItemOnClickListener listener;

    public void setOnSensorItemClick(SensorItemOnClickListener listener) {
        this.listener = listener;
    }

    public SensorItem(Context context) {
        this(context, null);
    }

    public SensorItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        mIconFont = FontManager.getTypeface(context, FontManager.PLANT_FACTORY_FONT);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ControllerItem, 0, 0);
        String iconCode = a.getString(R.styleable.ControllerItem_iconCode);
        String name = a.getString(R.styleable.ControllerItem_name);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_sensor_item, this, true);

        FontManager.markAsIconContainer(this, mIconFont);

        mName = (TextView) findViewById(R.id.tv_name);
        mIcon = (TextView) findViewById(R.id.tv_icon);
        mValue = (TextView) findViewById(R.id.tv_value);
        mUpdatedTime = (TextView) findViewById(R.id.tv_updated_time);
        mError = (TextView) findViewById(R.id.tv_error);

        mName.setText(name);
        mIcon.setText(iconCode);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SensorItem.this.listener != null) {
                    SensorItem.this.listener.onSensorClick(SensorItem.this.mItem);
                }
            }
        });
    }

    public void setValue(Sensor item) {
        this.mItem = item;
        String unit = UnitUtils.getUnitByType((int) item.sensor_type);
        if (!unit.isEmpty()) {
            unit = " (" + unit + ")";
        }
        mValue.setText(item.sensor_value + unit);
        mUpdatedTime.setText(getUpdatedTime(item.sensor_updated_date));
        if (item.is_error) {
            mError.setVisibility(View.VISIBLE);
        }
        else {
            mError.setVisibility(View.GONE);
        }

        if (item.sensor_type == SensorDefine.WATER_LEAK_SENSOR) {
            if (item.sensor_value == 1) {
                mValue.setText(getContext().getString(R.string.sensor_water_leak_leaked));
                mValue.setTextColor(Color.RED);
                mIcon.setTextColor(Color.RED);
            }
            else {
                mValue.setTextColor(ContextCompat.getColor(getContext(), R.color.colorTextPrimary));
                mIcon.setTextColor(ContextCompat.getColor(getContext(), R.color.colorTextPrimary));
            }
        }
    }

    private String getUpdatedTime(long updatedTime) {
        Date currentDate = new Date();
        long diff = currentDate.getTime() - updatedTime;

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = diff / daysInMilli;
        diff = diff % daysInMilli;

        long elapsedHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long elapsedMinutes = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long elapsedSeconds = diff / secondsInMilli;

        if (elapsedDays > 0) {
            return "(" + elapsedDays + " " + getContext().getString(R.string.updated_time_text_day) + ")";
        }

        if (elapsedHours > 0) {
            return "(" + elapsedHours + " " + getContext().getString(R.string.updated_time_text_hour) + ")";
        }

        if (elapsedMinutes > 0) {
            return "(" + elapsedMinutes + " " + getContext().getString(R.string.updated_time_text_minute) + ")";
        }
        else {
            return "(" + elapsedSeconds + " " + getContext().getString(R.string.updated_time_text_second) + ")";
        }
    }
}
