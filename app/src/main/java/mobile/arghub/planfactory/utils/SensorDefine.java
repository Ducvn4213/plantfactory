package mobile.arghub.planfactory.utils;

public class SensorDefine {
    public static final int UNKNOWN_SENSOR = 0;
    public static final int LIGHT_SENSOR = 1;
    public static final int AIR_TEMPERATURE_SENSOR = 2;
    public static final int AIR_HUMIDITY_SENSOR = 3;
    public static final int SOIL_TEMPERATURE_SENSOR = 4;
    public static final int SOIL_HUMIDITY_SENSOR = 5;
    public static final int SOIL_EC_SENSOR = 6;
    public static final int SOIL_PH_SENSOR = 7;
    public static final int WATER_TEMPERATURE_SENSOR = 8;
    public static final int WATER_EC_SENSOR = 9;
    public static final int WATER_PH_SENSOR = 10;
    public static final int WATER_ORP_SENSOR = 11;
    public static final int BATTERY_SENSOR = 12;
    public static final int CO2_SENSOR = 13;
    public static final int WATER_LEVEL_SENSOR = 14;
    public static final int WATER_LEAK_SENSOR = 15;
    public static final int ERROR_SENSOR = 16;
}
