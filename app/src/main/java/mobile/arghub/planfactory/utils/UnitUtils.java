package mobile.arghub.planfactory.utils;

public class UnitUtils {
    private static final String DEF_LIGHT_SENSOR_UNIT = "lux";
    private static final String DEF_TEMP_SENSOR_UNIT = "°C";
    private static final String DEF_HUMIDITY_SENSOR_UNIT = "%";
    private static final String DEF_PH_SENSOR_UNIT = "pH";
    private static final String DEF_CO2_SENSOR_UNIT = "ppm";
    private static final String DEF_EC_SENSOR_UNIT = "mS/cm";
    private static final String DEF_WATER_LEVEL_SENSOR_UNIT = "%";

    public static String getUnitByType(int type) {
        int _type = (int) type;
        switch (_type) {
            case SensorDefine.LIGHT_SENSOR:
                return DEF_LIGHT_SENSOR_UNIT;
            case SensorDefine.AIR_TEMPERATURE_SENSOR:
                return DEF_TEMP_SENSOR_UNIT;
            case SensorDefine.AIR_HUMIDITY_SENSOR:
                return DEF_HUMIDITY_SENSOR_UNIT;
            case SensorDefine.SOIL_TEMPERATURE_SENSOR:
                return DEF_TEMP_SENSOR_UNIT;
            case SensorDefine.SOIL_HUMIDITY_SENSOR:
                return DEF_HUMIDITY_SENSOR_UNIT;
            case SensorDefine.SOIL_EC_SENSOR:
                return DEF_EC_SENSOR_UNIT;
            case SensorDefine.SOIL_PH_SENSOR:
                return DEF_PH_SENSOR_UNIT;
            case SensorDefine.WATER_TEMPERATURE_SENSOR:
                return DEF_TEMP_SENSOR_UNIT;
            case SensorDefine.WATER_EC_SENSOR:
                return DEF_EC_SENSOR_UNIT;
            case SensorDefine.WATER_PH_SENSOR:
                return DEF_PH_SENSOR_UNIT;
            case SensorDefine.WATER_ORP_SENSOR:
                return "";
            case SensorDefine.BATTERY_SENSOR:
                return DEF_HUMIDITY_SENSOR_UNIT;
            case SensorDefine.CO2_SENSOR:
                return DEF_CO2_SENSOR_UNIT;
            case SensorDefine.WATER_LEVEL_SENSOR:
                return DEF_WATER_LEVEL_SENSOR_UNIT;
            case SensorDefine.WATER_LEAK_SENSOR:
                return "";
            case SensorDefine.ERROR_SENSOR:
                return "";
            default:
                break;
        }

        return "";
    }
}
