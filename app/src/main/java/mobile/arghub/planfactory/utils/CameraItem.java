package mobile.arghub.planfactory.utils;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;

import mobile.arghub.planfactory.R;

public class CameraItem extends LinearLayout implements IVLCVout.Callback {

    SurfaceView mSurface;
    ProgressBar mProgressBar;

    private SurfaceHolder holder;
    private LibVLC libvlc;
    private MediaPlayer mMediaPlayer = null;
    private int mVideoWidth;
    private int mVideoHeight;

    private String currentPresentURL;

    public CameraItem(Context context) {
        this(context, null);
    }

    public CameraItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_camera_item, this, true);

        mSurface = (SurfaceView) findViewById(R.id.sv_surface);
        mProgressBar = (ProgressBar) findViewById(R.id.pb_progress);

        initUI();
    }

    void initUI() {
        holder = mSurface.getHolder();
        //mProgressBar.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public void handleOnResume() {
        if (currentPresentURL != null) {
            setLink(currentPresentURL);
        }
    }

    public void setLink(String media) {
        currentPresentURL = media;
        mProgressBar.setVisibility(VISIBLE);
        releasePlayer();
        try {
            // Create LibVLC
            // TODO: make this more robust, and sync with audio demo
            ArrayList<String> options = new ArrayList<String>();
            //options.add("--subsdec-encoding <encoding>");
            options.add("--aout=opensles");
            options.add("--audio-time-stretch"); // time stretching
            options.add("-vvv"); // verbosity
            libvlc = new LibVLC(getContext(), options);
            if (holder == null) {
                holder = mSurface.getHolder();
            }
            holder.setKeepScreenOn(true);

            // Creating media player
            mMediaPlayer = new MediaPlayer(libvlc);
            mMediaPlayer.setEventListener(new MediaPlayer.EventListener() {
                @Override
                public void onEvent(MediaPlayer.Event event) {
                    switch (event.type) {
                        case MediaPlayer.Event.EndReached:
                            releasePlayer();
                            break;
                        case MediaPlayer.Event.Playing:
                            mProgressBar.setVisibility(GONE);
                            break;
                        case MediaPlayer.Event.Paused:
                        case MediaPlayer.Event.Stopped:
                        default:
                            break;
                    }
                }
            });

            // Seting up video output
            final IVLCVout vout = mMediaPlayer.getVLCVout();
            vout.setVideoView(mSurface);
            //vout.setSubtitlesView(mSurfaceSubtitles);
            vout.addCallback(this);
            vout.attachViews();

            Media m = new Media(libvlc, Uri.parse(media));
            mMediaPlayer.setMedia(m);
            mMediaPlayer.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void releasePlayer() {
        if (libvlc == null)
            return;
        mMediaPlayer.stop();
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.removeCallback(this);
        vout.detachViews();
        holder = null;
        libvlc.release();
        libvlc = null;

        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;

        if (holder == null || mSurface == null)
            return;

        float w = mSurface.getWidth();
        float h = mSurface.getHeight();

        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        if (videoAR > 1) {
            float ratio = h / mVideoHeight;
            w = mVideoWidth * ratio;
        }
        else {
            float ratio = w / mVideoWidth;
            h = mVideoHeight * ratio;
        }

        holder.setFixedSize(mVideoWidth, mVideoHeight);
        ViewGroup.LayoutParams lp = mSurface.getLayoutParams();
        lp.width = (int) w;
        lp.height = (int) h;
        mSurface.setLayoutParams(lp);
        mSurface.invalidate();
    }

    @Override
    public void onNewLayout(IVLCVout vout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;

        // store video size
        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vout) {
        //TODO
    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vout) {
        Log.d("AAAAAAAAAAAAAAAAA", "onEvent: ");
        //TODO
    }

    @Override
    public void onHardwareAccelerationError(IVLCVout vlcVout) {
        this.releasePlayer();
    }
}
