package mobile.arghub.planfactory.utils;

import android.content.Context;
import android.view.View;

import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.model.Sensor;

public class SensorUtils {
    private SensorItem lightSensor;
    private SensorItem airTempSensor;
    private SensorItem airHumiditySensor;
    private SensorItem soilTempSensor;
    private SensorItem soilHumiditySensor;
    private SensorItem soilECSensor;
    private SensorItem soilPHSensor;
    private SensorItem waterTempSensor;
    private SensorItem waterECSensor;
    private SensorItem waterPHSensor;
    private SensorItem waterORPSensor;
    private SensorItem batterySensor;
    private SensorItem cO2Sensor;
    private SensorItem waterLevelSensor;
    private SensorItem waterLeakSensor;
    private SensorItem errorSensor;

    private Sensor lightSensorValue;
    private Sensor airTempSensorValue;
    private Sensor airHumiditySensorValue;
    private Sensor soilTempSensorValue;
    private Sensor soilHumiditySensorValue;
    private Sensor soilECSensorValue;
    private Sensor soilPHSensorValue;
    private Sensor waterTempSensorValue;
    private Sensor waterECSensorValue;
    private Sensor waterPHSensorValue;
    private Sensor waterORPSensorValue;
    private Sensor batterySensorValue1;
    private Sensor batterySensorValue2;
    private Sensor cO2SensorValue;
    private Sensor waterLevelSensorValue;
    private Sensor waterLeakSensorValue;
    private Sensor errorSensorValue;

    public void setLightSensor(SensorItem item) {
        this.lightSensor = item;
    }

    public void setAirTempSensor(SensorItem item) {
        this.airTempSensor = item;
    }

    public void setAirHumiditySensor(SensorItem item) {
        this.airHumiditySensor = item;
    }

    public void setSoilTempSensor(SensorItem item) {
        this.soilTempSensor = item;
    }

    public void setSoilHumiditySensor(SensorItem item) {
        this.soilHumiditySensor = item;
    }

    public void setSoilECSensor(SensorItem item) {
        this.soilECSensor = item;
    }

    public void setSoilPHSensor(SensorItem item) {
        this.soilPHSensor = item;
    }

    public void setWaterTempSensor(SensorItem item) {
        this.waterTempSensor = item;
    }

    public void setWaterECSensor(SensorItem item) {
        this.waterECSensor = item;
    }

    public void setWaterPHSensor(SensorItem item) {
        this.waterPHSensor = item;
    }

    public void setWaterORPSensor(SensorItem item) {
        this.waterORPSensor = item;
    }

    public void setBatterySensor(SensorItem item) {
        this.batterySensor = item;
    }

    public void setcO2Sensor(SensorItem item) {
        this.cO2Sensor = item;
    }

    public void setWaterLevelSensor(SensorItem item) {
        this.waterLevelSensor = item;
    }

    public void setWaterLeakSensor(SensorItem item) {
        this.waterLeakSensor = item;
    }

    public void setErrorSensor(SensorItem item) {
        this.errorSensor = item;
    }

    private static SensorUtils instance;
    private SensorUtils() {}

    public static SensorUtils getInstance() {
        if (instance == null) {
            instance = new SensorUtils();
        }

        return instance;
    }

    public void setValues(List<Sensor> data) {
        for (Sensor s : data ) {
            setValueForSensorByType(s.sensor_type, s);
        }
    }

    void setValueForSensorByType(long type, Sensor value) {
        int _type = (int) type;
        switch (_type) {
            case SensorDefine.LIGHT_SENSOR:
                lightSensorValue = value;
                return;
            case SensorDefine.AIR_TEMPERATURE_SENSOR:
                airTempSensorValue = value;
                return;
            case SensorDefine.AIR_HUMIDITY_SENSOR:
                airHumiditySensorValue = value;
                return;
            case SensorDefine.SOIL_TEMPERATURE_SENSOR:
                soilTempSensorValue = value;
                return;
            case SensorDefine.SOIL_HUMIDITY_SENSOR:
                soilHumiditySensorValue = value;
                return;
            case SensorDefine.SOIL_EC_SENSOR:
                soilECSensorValue = value;
                return;
            case SensorDefine.SOIL_PH_SENSOR:
                soilPHSensorValue = value;
                return;
            case SensorDefine.WATER_TEMPERATURE_SENSOR:
                waterTempSensorValue = value;
                return;
            case SensorDefine.WATER_EC_SENSOR:
                waterECSensorValue = value;
                return;
            case SensorDefine.WATER_PH_SENSOR:
                waterPHSensorValue = value;
                return;
            case SensorDefine.WATER_ORP_SENSOR:
                waterORPSensorValue = value;
                return;
            case SensorDefine.BATTERY_SENSOR:
                if (value.deviceName == 5) {
                    batterySensorValue1 = value;
                }
                else {
                    batterySensorValue2 = value;
                }
                return;
            case SensorDefine.CO2_SENSOR:
                cO2SensorValue = value;
                return;
            case SensorDefine.WATER_LEVEL_SENSOR:
                waterLevelSensorValue = value;
                return;
            case SensorDefine.WATER_LEAK_SENSOR:
                waterLeakSensorValue = value;
                return;
            case SensorDefine.ERROR_SENSOR:
                errorSensorValue = value;
                return;
            default:
                break;
        }
    }

    public static boolean needToShowPIN(Sensor item) {
        int _type = (int) item.sensor_type;
        switch (_type) {
            case SensorDefine.LIGHT_SENSOR:
                return true;
            case SensorDefine.AIR_TEMPERATURE_SENSOR:
                return true;
            case SensorDefine.AIR_HUMIDITY_SENSOR:
                return true;
            case SensorDefine.SOIL_TEMPERATURE_SENSOR:
                return false;
            case SensorDefine.SOIL_HUMIDITY_SENSOR:
                return false;
            case SensorDefine.SOIL_EC_SENSOR:
                return false;
            case SensorDefine.SOIL_PH_SENSOR:
                return false;
            case SensorDefine.WATER_TEMPERATURE_SENSOR:
                return false;
            case SensorDefine.WATER_EC_SENSOR:
                return false;
            case SensorDefine.WATER_PH_SENSOR:
                return false;
            case SensorDefine.WATER_ORP_SENSOR:
                return false;
            case SensorDefine.BATTERY_SENSOR:
                return false;
            case SensorDefine.CO2_SENSOR:
                return false;
            case SensorDefine.WATER_LEVEL_SENSOR:
                return false;
            case SensorDefine.WATER_LEAK_SENSOR:
                return false;
            case SensorDefine.ERROR_SENSOR:
                return false;
            default:
                break;
        }

        return false;
    }

    public void prepareForUpdateData() {
        lightSensorValue = null;
        airTempSensorValue = null;
        airHumiditySensorValue = null;
        soilTempSensorValue = null;
        soilHumiditySensorValue = null;
        soilECSensorValue = null;
        soilPHSensorValue = null;
        waterTempSensorValue = null;
        waterECSensorValue = null;
        waterPHSensorValue = null;
        waterORPSensorValue = null;
        batterySensorValue1 = null;
        batterySensorValue2 = null;
        cO2SensorValue = null;
        waterLevelSensorValue = null;
        waterLeakSensorValue = null;
        errorSensorValue = null;
    }

    public void updateData() {
        setValueFor(lightSensor, lightSensorValue);
        setValueFor(airTempSensor, airTempSensorValue);
        setValueFor(airHumiditySensor, airHumiditySensorValue);
        setValueFor(soilTempSensor, soilTempSensorValue);
        setValueFor(soilHumiditySensor, soilHumiditySensorValue);
        setValueFor(soilECSensor, soilECSensorValue);
        setValueFor(soilPHSensor, soilPHSensorValue);
        setValueFor(waterTempSensor, waterTempSensorValue);
        setValueFor(waterECSensor, waterECSensorValue);
        setValueFor(waterPHSensor, waterPHSensorValue);
        setValueFor(waterORPSensor, waterORPSensorValue);
        //setValueFor(batterySensor, batterySensorValue);
        setValueFor(cO2Sensor, cO2SensorValue);
        setValueFor(waterLevelSensor, waterLevelSensorValue);
        //setValueFor(waterLeakSensor, waterLeakSensorValue);
        setValueFor(errorSensor, errorSensorValue);
    }

    private void setValueFor(SensorItem item, Sensor value) {
        if (value!= null) {
            item.setValue(value);
            item.setVisibility(View.VISIBLE);
        }
        else {
            item.setVisibility(View.GONE);
        }
    }

    public String getSensorNameByType(Context context, long type) {
        int _type = (int) type;
        switch (_type) {
            case SensorDefine.LIGHT_SENSOR:
                return context.getString(R.string.sensor_light);
            case SensorDefine.AIR_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_air_temperature);
            case SensorDefine.AIR_HUMIDITY_SENSOR:
                return context.getString(R.string.sensor_air_humidity);
            case SensorDefine.SOIL_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_soil_temperature);
            case SensorDefine.SOIL_HUMIDITY_SENSOR:
                return context.getString(R.string.sensor_soil_humidity);
            case SensorDefine.SOIL_EC_SENSOR:
                return context.getString(R.string.sensor_soil_ec);
            case SensorDefine.SOIL_PH_SENSOR:
                return context.getString(R.string.sensor_soil_ph);
            case SensorDefine.WATER_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_water_temperature);
            case SensorDefine.WATER_EC_SENSOR:
                return context.getString(R.string.sensor_water_ec);
            case SensorDefine.WATER_PH_SENSOR:
                return context.getString(R.string.sensor_water_ph);
            case SensorDefine.WATER_ORP_SENSOR:
                return context.getString(R.string.sensor_water_orp);
            case SensorDefine.BATTERY_SENSOR:
                return context.getString(R.string.sensor_battery);
            case SensorDefine.CO2_SENSOR:
                return context.getString(R.string.sensor_co2);
            case SensorDefine.WATER_LEVEL_SENSOR:
                return context.getString(R.string.sensor_water_level);
            case SensorDefine.WATER_LEAK_SENSOR:
                return context.getString(R.string.sensor_water_leak);
            case SensorDefine.ERROR_SENSOR:
                return context.getString(R.string.sensor_error);
            default:
                break;
        }

        return "";
    }

    public String getBatteryLevel(long deviceName) {
        if (deviceName == 5) {
            if (batterySensorValue1 == null) {
                return "--";
            }

            return batterySensorValue1.sensor_value + "%";
        }
        else {
            if (batterySensorValue2 == null) {
                return "--";
            }

            return batterySensorValue2.sensor_value + "%";
        }
    }
}
