package mobile.arghub.planfactory.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.Sensor;
import mobile.arghub.planfactory.service.network.Param;

public class PercentItem extends LinearLayout {

    private TextView mTitle;
    private TextView mCurrentValue;
    private View mPercentView;
    private View mPercentFullView;
    private Button mResetButton;

    private String factoryID;
    private String keyReset = "factory_liquid_level_";

    public PercentItem(Context context) {
        this(context, null);
    }

    public PercentItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SettingItem, 0, 0);
        String title = a.getString(R.styleable.SettingItem_title);
        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_percent_item, this, true);

        mTitle = (TextView) findViewById(R.id.tv_title);
        mCurrentValue = (TextView) findViewById(R.id.tv_current_value);
        mPercentView = findViewById(R.id.v_percent);
        mPercentFullView = findViewById(R.id.v_percent_full);
        mResetButton = (Button) findViewById(R.id.btn_reset);

        mTitle.setText(title);

        keyReset += title.toLowerCase();

        mResetButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mResetButton.setText("...");

                List<Param> params = new ArrayList<>();
                params.add(new Param(keyReset, "100"));
                PFService.getInstance().saveSetting(factoryID, params, new PFService.Callback<String>() {
                    @Override
                    public void onSuccess(String data) {
                        Handler mainHandler = new Handler(context.getMainLooper());

                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                setValue(100);
                                mResetButton.setText(context.getText(R.string.button_reset));
                            }
                        };
                        mainHandler.post(myRunnable);
                    }

                    @Override
                    public void onFail(String error) {
                        Handler mainHandler = new Handler(context.getMainLooper());

                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                mResetButton.setText(context.getText(R.string.button_reset));
                            }
                        };
                        mainHandler.post(myRunnable);
                    }
                });
            }
        });
    }

    private void setValue(long percent) {
        if (percent <= 10) {
            mResetButton.setVisibility(VISIBLE);
        }
        else {
            mResetButton.setVisibility(GONE);
        }

        mCurrentValue.setText(percent + "%");

        float value100 = mPercentFullView.getWidth();
        float valueCurrent = value100 * percent / 100;

        ViewGroup.LayoutParams layoutParams = mPercentView.getLayoutParams();
        layoutParams.width = (int) valueCurrent;

        mPercentView.setLayoutParams(layoutParams);
    }

    public void setValue(long factoryID, long percent) {
        this.factoryID = factoryID + "";
        setValue(percent);
    }
}
