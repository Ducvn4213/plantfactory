package mobile.arghub.planfactory.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.WindowManager;

import com.squareup.picasso.Transformation;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import mobile.arghub.planfactory.R;

import static android.content.Context.MODE_PRIVATE;

public class Utils {

    public static final int DiaryType_UTM = 0;
    public static final int DiaryType_Hourly = 1;
    public static final int DiaryType_Daily = 2;
    public static final int DiaryType_Weekly = 3;
    public static final int DiaryType_Monthly = 4;


    private static final String PLANT_FACTORY_PREFS_NAME = "PLANT_FACTORY_PREFS_NAME";
    private static final String PREFS_KEY_FIRST_USING_APP = "PREFS_KEY_FIRST_USING_APP";

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static void savePreference(Context context, String key, String data) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key, data);
        editor.commit();
    }

    public static String getPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(key, "");

        if (restoredText.trim().isEmpty()) {
            return null;
        }

        return restoredText;
    }

    public static Boolean isFirstUsingApp(Context context) {
        String keyCheck = getPreference(context, PREFS_KEY_FIRST_USING_APP);
        return keyCheck == null;
    }

    public static void showErrorDialog(Context context, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.button_error)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static void showDialog(Context context, String title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        int theSize = Math.min(bitmap.getWidth(), bitmap.getHeight());
        final Bitmap output = Bitmap.createBitmap(theSize,
                theSize, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, theSize, theSize);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        //bitmap.recycle();

        return output;
    }

    public static void hideKeyboard(@NonNull Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String encode(String source) {
        byte[] data = source.getBytes(StandardCharsets.UTF_8);
        String returnValue = Base64.encodeToString(data, Base64.DEFAULT);
        return returnValue;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String decode(String source) {
        byte[] data = Base64.decode(source, Base64.DEFAULT);
        String returnValue = new String(data, StandardCharsets.UTF_8);
        return returnValue;
    }

    public interface ConvertBitmapToStringCallback {
        void onSuccess(String bitmapString);
        void onFail();
    }

    private static String bitMapToString(Bitmap bitmap){
        //Bitmap scaleBitmap = bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte [] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, 0);
        temp = temp.replace("\n", "").replace("\r", "");
        return temp;
    }

    public static class ConvertBitmapToStringTask extends AsyncTask<String, Void, String> {

        private Bitmap mBitmap;
        private ConvertBitmapToStringCallback mCallback;

        public void setBitmap(Bitmap bitmap) {
            this.mBitmap = bitmap;
        }

        public void setCallback(ConvertBitmapToStringCallback callback) {
            this.mCallback = callback;
        }

        protected String doInBackground(String... bitmap) {
            return bitMapToString(this.mBitmap);
        }

        protected void onPostExecute(String result) {
            if (mCallback != null) {
                mCallback.onSuccess(result);
            }
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
