package mobile.arghub.planfactory.utils;

import android.content.Context;
import android.view.View;

import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.model.Controller;

public class ControllerUtils {
    private ControllerItem lampController;
    private ControllerItem mistingPumpController;
    private ControllerItem fanController;
    //private ControllerItem airConditionerController;
    //private ControllerItem cO2Controller;
    private ControllerItem dosingPumpController;
    private ControllerItem waterPumpController;
    //private ControllerItem oxygenPumpController;
    private ControllerItem washingModeController;

    private Boolean waterPumpControllerValue;
    private Boolean oxygenPumpControllerValue;
    private Boolean washingModeControllerValue;
    private Boolean lampControllerValue;
    private Boolean mistingPumpControllerValue;
    private Boolean fanControllerValue;
    private Boolean airConditionerControllerValue;
    private Boolean cO2ControllerValue;
    private Boolean dosingPumpControllerValue;

    private static ControllerUtils instance;
    private ControllerUtils() {}

    public static ControllerUtils getInstance() {
        if (instance == null) {
            instance = new ControllerUtils();
        }

        return instance;
    }

    public void setLampController(ControllerItem item) {
        this.lampController = item;
    }

    public void setMistingPumpController(ControllerItem item) {
        this.mistingPumpController = item;
    }

    public void setFanController(ControllerItem item) {
        this.fanController = item;
    }

    private void setAirConditionerController(ControllerItem item) {
        //this.airConditionerController = item;
    }

    private void setCO2Controller(ControllerItem item) {
        //this.cO2Controller = item;
    }

    public void setDosingPumpController(ControllerItem item) {
        this.dosingPumpController = item;
    }

    public void setLampControllerValue(Boolean value) {
        this.lampControllerValue = value;
    }

    public void setMistingPumpControllerValue(Boolean value) {
        this.mistingPumpControllerValue = value;
    }

    public void setFanControllerValue(Boolean value) {
        this.fanControllerValue = value;
    }

    public void setAirConditionerControllerValue(Boolean value) {
        this.airConditionerControllerValue = value;
    }

    public void setCO2ControllerValue(Boolean value) {
        this.cO2ControllerValue = value;
    }

    public void setDosingPumpControllerValue(Boolean value) {
        this.dosingPumpControllerValue = value;
    }

    public void setWaterPumpController(ControllerItem item) {
        this.waterPumpController = item;
    }

    private void setOxygenPumpController(ControllerItem item) {
        //this.oxygenPumpController = item;
    }

    public void setWashingModeController(ControllerItem item) {
        this.washingModeController = item;
    }

    public void setWaterPumpControllerValue(Boolean value) {
        this.waterPumpControllerValue = value;
    }

    public void setOxygenPumpControllerValue(Boolean value) {
        this.oxygenPumpControllerValue = value;
    }

    public void setWashingModeControllerValue(Boolean value) {
        this.washingModeControllerValue = value;
    }

    public void prepareForUpdateData() {
        lampControllerValue = null;
        mistingPumpControllerValue = null;
        fanControllerValue = null;
        airConditionerControllerValue = null;
        cO2ControllerValue = null;
        dosingPumpControllerValue = null;
        waterPumpControllerValue = null;
        oxygenPumpControllerValue = null;
        washingModeControllerValue = null;

        lampController.setItem(null);
        mistingPumpController.setItem(null);
        fanController.setItem(null);
        //airConditionerController.setItem(null);
        //cO2Controller.setItem(null);
        dosingPumpController.setItem(null);
        waterPumpController.setItem(null);
        //oxygenPumpController.setItem(null);
        washingModeController.setItem(null);
    }

    public void setValues(List<Controller> data) {
        for (Controller c : data ) {
            setValueForControllerByType(c);
        }
    }

    void setValueForControllerByType(Controller controller) {
        int _type = (int) controller.controller_type;
        boolean value = controller.controller_is_on;
        switch (_type) {
            case ControllerDefine.LAMP_CONTROLLER:
                lampControllerValue = value;
                lampController.setItem(controller);
                return;
            case ControllerDefine.WATER_PUMP_CONTROLLER:
                waterPumpControllerValue = value;
                waterPumpController.setItem(controller);
                return;
            case ControllerDefine.MISTING_PUMP_CONTROLLER:
                mistingPumpControllerValue = value;
                mistingPumpController.setItem(controller);
                return;
            case ControllerDefine.FAN_CONTROLLER:
                fanControllerValue = value;
                fanController.setItem(controller);
                return;
            case ControllerDefine.CO2_CONTROLLER:
                cO2ControllerValue = value;
                //cO2Controller.setItem(controller);
                return;
            case ControllerDefine.AIR_CONDITIONER_CONTROLLER:
                airConditionerControllerValue = value;
                //airConditionerController.setItem(controller);
                return;
            case ControllerDefine.DOSING_PUMP_CONTROLLER:
                dosingPumpControllerValue = value;
                dosingPumpController.setItem(controller);
                return;
            case ControllerDefine.OXYGEN_PUMP_CONTROLLER:
                oxygenPumpControllerValue = value;
                //oxygenPumpController.setItem(controller);
                return;
            case ControllerDefine.WASHING_MODE_CONTROLLER:
                washingModeControllerValue = value;
                washingModeController.setItem(controller);
                return;
            default:
                break;
        }
    }

    public void updateData() {
        setValueFor(lampController, lampControllerValue);
        setValueFor(mistingPumpController, mistingPumpControllerValue);
        setValueFor(fanController, fanControllerValue);
        //setValueFor(airConditionerController, airConditionerControllerValue);
        //setValueFor(cO2Controller, cO2ControllerValue);
        setValueFor(dosingPumpController, dosingPumpControllerValue);
        setValueFor(waterPumpController, waterPumpControllerValue);
        //setValueFor(oxygenPumpController, oxygenPumpControllerValue);
        setValueFor(washingModeController, washingModeControllerValue);
    }

    private void setValueFor(ControllerItem item, Boolean value) {
        if (item == null) {
            return;
        }

        if (value != null) {
            item.isHumanGesture = false;
            item.setValueForSwitcher(value);
            item.setVisibility(View.VISIBLE);
        }
        else {
            item.setVisibility(View.GONE);
        }
    }

    public String getNameOfControllerByType(Context context, long type) {
        int _type = (int) type;
        switch (_type) {
            case ControllerDefine.LAMP_CONTROLLER:
                return context.getString(R.string.controller_lamp);
            case ControllerDefine.WATER_PUMP_CONTROLLER:
                return context.getString(R.string.controller_water_pump);
            case ControllerDefine.MISTING_PUMP_CONTROLLER:
                return context.getString(R.string.controller_misting_pump);
            case ControllerDefine.FAN_CONTROLLER:
                return context.getString(R.string.controller_fan);
            case ControllerDefine.CO2_CONTROLLER:
                return context.getString(R.string.controller_co2);
            case ControllerDefine.AIR_CONDITIONER_CONTROLLER:
                return context.getString(R.string.controller_air_conditioner);
            case ControllerDefine.DOSING_PUMP_CONTROLLER:
                return context.getString(R.string.controller_dosing_pump);
            case ControllerDefine.OXYGEN_PUMP_CONTROLLER:
                return context.getString(R.string.controller_oxygen_pump);
            case ControllerDefine.WASHING_MODE_CONTROLLER:
                return context.getString(R.string.controller_washing_mode);
            case ControllerDefine.VALVE_INPUT:
                return context.getString(R.string.controller_value_input);
            case ControllerDefine.VALVE_OUTPUT:
                return context.getString(R.string.controller_value_output);
            default:
                break;
        }

        return "";
    }
}
