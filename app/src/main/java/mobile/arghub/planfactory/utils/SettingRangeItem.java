package mobile.arghub.planfactory.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.Controller;

public class SettingRangeItem extends LinearLayout {

    private TextView mName;
    private TextView mMinValue;
    private TextView mMaxValue;
    private RangeSeekBar mRanger;
    Context mContext;

    private RangeSeekBar.OnRangeSeekBarChangeListener delegate;

    public void setDelegate(RangeSeekBar.OnRangeSeekBarChangeListener delegate) {
        this.delegate = delegate;

        if (mRanger != null) {
            mRanger.setOnRangeSeekBarChangeListener(this.delegate);
        }
    }

    public SettingRangeItem(Context context) {
        this(context, null);
    }

    public SettingRangeItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SettingItem, 0, 0);
        String title = a.getString(R.styleable.SettingItem_title);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_setting_range_item, this, true);

        mName = (TextView) findViewById(R.id.tv_name);
        mRanger = (RangeSeekBar) findViewById(R.id.rs_value);
        mMinValue = (TextView) findViewById(R.id.tv_min_value);
        mMaxValue = (TextView) findViewById(R.id.tv_max_value);

        mName.setText(title);
    }

    public void setRange(int min, int max) {
        if (mRanger != null) {
            mRanger.setRangeValues(min, max);
        }

        mMinValue.setText(min + "");
        mMaxValue.setText(max + "");
    }

    public void setRange(double min, double max) {
        if (mRanger != null) {
            mRanger.setRangeValues(min, max);
        }

        mMinValue.setText(min + "");
        mMaxValue.setText(max + "");
    }

    public void setCurrentMinValue(int data) {
        if (mRanger != null) {
            mRanger.setSelectedMinValue(data);
        }
    }

    public void setCurrentMaxValue(int data) {
        if (mRanger != null) {
            mRanger.setSelectedMaxValue(data);
        }
    }

    public void setCurrentMinValue(double data) {
        if (mRanger != null) {
            mRanger.setSelectedMinValue(data);
        }
    }

    public void setCurrentMaxValue(double data) {
        if (mRanger != null) {
            mRanger.setSelectedMaxValue(data);
        }
    }

    public int getCurrentMinValue() {
        return (int) mRanger.getSelectedMinValue();
    }

    public double getDoubleCurrentMinValue() {
        return (double) mRanger.getSelectedMinValue();
    }

    public int getCurrentMaxValue() {
        return (int) mRanger.getSelectedMaxValue();
    }

    public double getDoubleCurrentMaxValue() {
        return (double) mRanger.getSelectedMaxValue();
    }
}
