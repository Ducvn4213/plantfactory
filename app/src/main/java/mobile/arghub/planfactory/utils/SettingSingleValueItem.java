package mobile.arghub.planfactory.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import mobile.arghub.planfactory.R;

public class SettingSingleValueItem extends LinearLayout {

    private TextView mName;
    private TextView mMinValue;
    private TextView mMaxValue;
    private RangeSeekBar mRanger;
    Context mContext;

    private RangeSeekBar.OnRangeSeekBarChangeListener delegate;

    public void setDelegate(RangeSeekBar.OnRangeSeekBarChangeListener delegate) {
        this.delegate = delegate;

        if (mRanger != null) {
            mRanger.setOnRangeSeekBarChangeListener(this.delegate);
        }
    }

    public SettingSingleValueItem(Context context) {
        this(context, null);
    }

    public SettingSingleValueItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SettingItem, 0, 0);
        String title = a.getString(R.styleable.SettingItem_title);

        a.recycle();
        
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_setting_single_value_item, this, true);

        mName = (TextView) findViewById(R.id.tv_name);
        mRanger = (RangeSeekBar) findViewById(R.id.rs_value);
        mMinValue = (TextView) findViewById(R.id.tv_min_value);
        mMaxValue = (TextView) findViewById(R.id.tv_max_value);

        mName.setText(title);
    }

    public void setRange(int min, int max) {
        if (mRanger != null) {
            mRanger.setRangeValues(min, max);
        }

        mMinValue.setText(min + "");
        mMaxValue.setText(max + "");
    }

    public void setCurrentValue(int data) {
        if (mRanger != null) {
            mRanger.setSelectedMaxValue(data);
        }
    }

    public int getCurrentValue() {
        return (int) mRanger.getSelectedMaxValue();
    }
}
