package mobile.arghub.planfactory.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import mobile.arghub.planfactory.R;

public class SettingSingleValueEditItem extends LinearLayout {

    private TextView mName;
    private TextView mValue;
    Context mContext;

    String unit = "";
    int currentValue;

    public SettingSingleValueEditItem(Context context) {
        this(context, null);
    }

    public SettingSingleValueEditItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SettingItem, 0, 0);
        String title = a.getString(R.styleable.SettingItem_title);
        String unit = a.getString(R.styleable.SettingItem_unit);

        a.recycle();
        
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_setting_single_value_edit_item, this, true);

        mName = (TextView) findViewById(R.id.tv_name);
        mValue = (TextView) findViewById(R.id.tv_value);

        mName.setText(title);
        this.unit = unit;

        mValue.setText("-- (" + unit + ")");

        setupControlEvents();
    }

    void setupControlEvents() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.layout_change_setting_single_edit_value);

                TextView title = (TextView) dialog.findViewById(R.id.tv_title);
                final EditText value = (EditText) dialog.findViewById(R.id.edt_value);
                Button ok = (Button) dialog.findViewById(R.id.btn_ok);

                title.setText(mName.getText().toString());
                value.setText(currentValue + "");

                ok.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            setValue(Integer.parseInt(value.getText().toString()));
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public void setValue(int value) {
        currentValue = value;
        mValue.setText(value + " (" + unit + ")");
    }

    public int getCurrentValue() {
        return currentValue;
    }
}
