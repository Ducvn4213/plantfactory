package mobile.arghub.planfactory.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import mobile.arghub.planfactory.R;

public class SettingSetCronTimeItem extends LinearLayout {

    private TextView mName;
    private TextView mValue;
    private Button mChangeButton;

    Context mContext;

    Calendar myCalendar = Calendar.getInstance();
    Dialog cronTimeDialog;

    AppCompatSpinner cronTimeSpinnerMinute;
    AppCompatSpinner cronTimeSpinnerHour;
    AppCompatSpinner cronTimeSpinnerDay;
    AppCompatSpinner cronTimeSpinnerMonth;
    AppCompatSpinner cronTimeSpinnerDayWeek;
    AppCompatSpinner cronTimeSpinnerYear;

    private Button cronTimeOKButton;
    private Button cronTimeCancelButton;

    private String currentCronValue = "";

    private RangeSeekBar.OnRangeSeekBarChangeListener delegate;

    public SettingSetCronTimeItem(Context context) {
        this(context, null);
    }

    public SettingSetCronTimeItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SettingItem, 0, 0);
        String title = a.getString(R.styleable.SettingItem_title);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_setting_set_cron_time_item, this, true);

        mName = (TextView) findViewById(R.id.tv_name);
        mValue = (TextView) findViewById(R.id.tv_value);

        mChangeButton = (Button) findViewById(R.id.btn_change_value);

        mName.setText(title);

        mChangeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                requestChangeValue();
            }
        });
    }

    public void setValue(String data) {
        if (!data.isEmpty()) {
            currentCronValue = data;
            mValue.setText(getStringFromCron(data));
        }
    }

    void requestChangeValue() {
        if (cronTimeDialog == null) {
            initCronTimeDialog();
        }

        cronTimeDialog.show();
    }

    void initCronTimeDialog() {
        cronTimeDialog = new Dialog(mContext);
        cronTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cronTimeDialog.setCancelable(true);
        cronTimeDialog.setContentView(R.layout.layout_cron_time_picker);

        cronTimeSpinnerMinute = (AppCompatSpinner) cronTimeDialog.findViewById(R.id.spn_cron_time_minute);
        cronTimeSpinnerHour = (AppCompatSpinner) cronTimeDialog.findViewById(R.id.spn_cron_time_hour);
        cronTimeSpinnerDay = (AppCompatSpinner) cronTimeDialog.findViewById(R.id.spn_cron_time_day);
        cronTimeSpinnerMonth = (AppCompatSpinner) cronTimeDialog.findViewById(R.id.spn_cron_time_month);
        cronTimeSpinnerDayWeek = (AppCompatSpinner) cronTimeDialog.findViewById(R.id.spn_cron_time_day_week);
        cronTimeSpinnerYear = (AppCompatSpinner) cronTimeDialog.findViewById(R.id.spn_cron_time_year);

        cronTimeCancelButton = (Button) cronTimeDialog.findViewById(R.id.btn_cancel);
        cronTimeOKButton = (Button) cronTimeDialog.findViewById(R.id.btn_ok);

        initSpinnerValue();
    }

    void initSpinnerValue() {
        List<String> minuteData = getDataWithRange(mContext.getString(R.string.setting_cron_time_picker_minute_default), 0, 59);
        List<String> hourData = getDataWithRange(mContext.getString(R.string.setting_cron_time_picker_hour_default), 0, 23);
        List<String> dayData = getDataWithRange(mContext.getString(R.string.setting_cron_time_picker_day_default), 1, 31);
        List<String> monthData = getDataWithRange(mContext.getString(R.string.setting_cron_time_picker_month_default), 1, 12);
        List<String> dayWeekData = getDataDayWeek(mContext.getString(R.string.setting_cron_time_picker_day_week_default));
        List<String> yearData = getDataWithRange(mContext.getString(R.string.setting_cron_time_picker_year_default), 1900, 3000);

        ArrayAdapter<String> minute_adapter = getAdapterFromData(minuteData);
        ArrayAdapter<String> hour_adapter = getAdapterFromData(hourData);
        ArrayAdapter<String> day_adapter = getAdapterFromData(dayData);
        ArrayAdapter<String> month_adapter = getAdapterFromData(monthData);
        ArrayAdapter<String> day_week_adapter = getAdapterFromData(dayWeekData);
        ArrayAdapter<String> year_adapter = getAdapterFromData(yearData);

        cronTimeSpinnerMinute.setAdapter(minute_adapter);
        cronTimeSpinnerHour.setAdapter(hour_adapter);
        cronTimeSpinnerDay.setAdapter(day_adapter);
        cronTimeSpinnerMonth.setAdapter(month_adapter);
        cronTimeSpinnerDayWeek.setAdapter(day_week_adapter);
        cronTimeSpinnerYear.setAdapter(year_adapter);

        cronTimeCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cronTimeDialog.isShowing()) {
                    cronTimeDialog.dismiss();
                }
            }
        });

        cronTimeOKButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleResult();
                if (cronTimeDialog.isShowing()) {
                    cronTimeDialog.dismiss();
                }
            }
        });
    }

    void handleResult() {
        String minute = getCronFor(cronTimeSpinnerMinute);
        String hour = getCronFor(cronTimeSpinnerHour);
        String day = getCronFor(cronTimeSpinnerDay);
        String month = getCronFor(cronTimeSpinnerMonth);
        String dayWeek = getCronForDayWeek();
        String year = getCronFor(cronTimeSpinnerYear);

        String cronString = minute + " " + hour + " " + day + " " + month + " " + dayWeek + " " + year;
        currentCronValue = cronString;
        mValue.setText(getStringFromCron(cronString));
    }

    String getStringFromCron(String cronString) {
        String[] crons = cronString.split(" ");

        String minute = crons[0];
        String hour = crons[1];
        String day = crons[2];
        String month = crons[3];
        String dayWeek = crons[4];
        String year = crons[5];

        String results = "";

        if (minute.equalsIgnoreCase("*")) {
            results += mContext.getString(R.string.cron_time_every_minutes) + "\n";
        }
        else {
            results += mContext.getString(R.string.cron_time_minute) + " " + minute + "\n";
        }

        if (hour.equalsIgnoreCase("*")) {
            results += mContext.getString(R.string.cron_time_every_hour) + "\n";
        }
        else {
            results += mContext.getString(R.string.cron_time_at) + " " + hour + " " +  mContext.getString(R.string.cron_time_hour) + "\n";
        }

//        if (day.equalsIgnoreCase("*")) {
//            results += "Mọi ngày\n";
//        }
//        else {
//            results += "Mỗi ngày " + day + "\n";
//        }

//        if (month.equalsIgnoreCase("*")) {
//            results += "Mọi tháng\n";
//        }
//        else {
//            results += "Mỗi tháng " + month + "\n";
//        }

        if (dayWeek.equalsIgnoreCase("*")) {
            results += mContext.getString(R.string.cron_time_week)  + "\n";
        }
        else {
            int dayWeekInt = Integer.parseInt(dayWeek);
            if (dayWeekInt == 7) {
                results += mContext.getString(R.string.cron_time_sunday) + "\n";
            }
            else {
                List<String> dayWeekData = getDataDayWeek(mContext.getString(R.string.setting_cron_time_picker_day_week_default));
                results += mContext.getString(R.string.cron_time_every_day_of_week) + " " + dayWeekData.get (dayWeekInt) + " " + mContext.getString(R.string.cron_time_in_week) + "\n";
            }
        }

//        if (year.equalsIgnoreCase("*")) {
//            results += "Tất cả các năm\n";
//        }
//        else {
//            results += "Năm " + year + "\n";
//        }

        return results;
    }

    String getCronForDayWeek() {
        int pos = cronTimeSpinnerDayWeek.getSelectedItemPosition();

        if (pos == 0) {
            return "*";
        }

        return pos + "";
    }

    String getCronFor(AppCompatSpinner spinner) {
        int pos = spinner.getSelectedItemPosition();

        if (pos == 0) {
            return "*";
        }

        return spinner.getSelectedItem().toString();
    }

    ArrayAdapter<String> getAdapterFromData(List<String> data) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.layout_spinner_item, R.id.tv_item_text, data);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);

        return adapter;
    }

    List<String> getDataDayWeek(String firstValue) {
        List<String> data = new ArrayList<>();
        data.add(firstValue);

        String[] items = getResources().getStringArray(R.array.array_day_week);
        Collections.addAll(data, items);

        return data;
    }

    List<String> getDataWithRange(String firstValue, int min, int max) {
        List<String> data = new ArrayList<>();

        data.add(firstValue);
        for (int i = min; i <= max; i++) {
            data.add(i + "");
        }

        return data;
    }

    public String getCurrentCronValue() {
        return currentCronValue;
    }
}
