package mobile.arghub.planfactory.utils;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.Calendar;

import mobile.arghub.planfactory.R;

public class SettingSetTimeItem extends LinearLayout {

    private TextView mName;
    private TextView mStartValue;
    private TextView mEndValue;
    private Button mStartButton;
    private Button mEndButton;
    Context mContext;

    long currentStartTime;
    long currentEndTime;

    Calendar myCalendar = Calendar.getInstance();

    private RangeSeekBar.OnRangeSeekBarChangeListener delegate;

    public SettingSetTimeItem(Context context) {
        this(context, null);
    }

    public SettingSetTimeItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SettingItem, 0, 0);
        String title = a.getString(R.styleable.SettingItem_title);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_setting_set_time_item, this, true);

        mName = (TextView) findViewById(R.id.tv_name);
        mStartValue = (TextView) findViewById(R.id.tv_start_value);
        mEndValue = (TextView) findViewById(R.id.tv_end_value);

        mStartButton = (Button) findViewById(R.id.btn_change_start_value);
        mEndButton = (Button) findViewById(R.id.btn_change_end_value);

        mName.setText(title);

        mStartButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(mContext, startTimePickerListener, myCalendar
                        .get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE),
                        true).show();
            }
        });

        mEndButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(mContext, endTimePickerListener, myCalendar
                        .get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE),
                        true).show();
            }
        });
    }

    public void setStartTime(long data) {
        currentStartTime = data;
        mStartValue.setText(getTimeString(data));
    }

    public void setEndTime(long data) {
        currentEndTime = data;
        mEndValue.setText(getTimeString(data));
    }

    private String getTimeString(long data) {
        int hour = (int) data / 60;
        int minutes = (int) data % 60;

        String hourString = hour + "";
        if (hour < 10) {
            hourString = "0" + hourString;
        }

        String minutestString = minutes + "";
        if (minutes < 10) {
            minutestString = "0" + minutestString;
        }

        return hourString + ":" + minutestString;
    }

    private TimePickerDialog.OnTimeSetListener startTimePickerListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
            int hour = selectedHour * 60;
            int minutes = selectedMinute;
            currentStartTime = hour + minutes;
            mStartValue.setText(new StringBuilder().append(pad(selectedHour))
                    .append(":").append(pad(selectedMinute)));

        }
    };

    private TimePickerDialog.OnTimeSetListener endTimePickerListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
            int hour = selectedHour * 60;
            int minutes = selectedMinute;
            currentEndTime = hour + minutes;
            mEndValue.setText(new StringBuilder().append(pad(selectedHour))
                    .append(":").append(pad(selectedMinute)));
        }
    };

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    public long getCurrentStartTimte() {
        return currentStartTime;
    }

    public long getCurrentEndTime() {
        return currentEndTime;
    }
}
