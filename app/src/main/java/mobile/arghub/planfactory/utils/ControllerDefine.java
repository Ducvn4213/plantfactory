package mobile.arghub.planfactory.utils;

public class ControllerDefine {
    public static final int UNKNOWN_CONTROLLER                = 0;
    public static final int LAMP_CONTROLLER                   = 1;
    public static final int WATER_PUMP_CONTROLLER             = 2;
    public static final int MISTING_PUMP_CONTROLLER           = 3;
    public static final int FAN_CONTROLLER                    = 4;
    public static final int AIR_CONDITIONER_CONTROLLER        = 5;
    public static final int CO2_CONTROLLER                    = 6;
    public static final int DOSING_PUMP_CONTROLLER            = 7;
    public static final int OXYGEN_PUMP_CONTROLLER            = 8;
    public static final int VALVE_INPUT                       = 9;
    public static final int VALVE_OUTPUT                      = 10;
    public static final int WASHING_MODE_CONTROLLER           = 11;
}
