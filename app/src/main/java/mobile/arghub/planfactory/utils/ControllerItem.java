package mobile.arghub.planfactory.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.Controller;

public class ControllerItem extends LinearLayout {

    private TextView mIcon;
    private TextView mError;
    private TextView mName;
    Typeface mIconFont;
    Context mContext;

    Controller mItem;
    ProgressDialog mProgressDialog;
    boolean isHumanGesture = false;
    boolean doNotUpdateValueForThis = false;

    boolean isOn = false;

    public void setItem(Controller data) {
        mItem = data;
        checkControllerError();
    }

    protected void showLoading() {
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog == null) {
                    mProgressDialog = new ProgressDialog(mContext);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setMessage(mContext.getString(R.string.progress_bar_message));
                }

                mProgressDialog.show();
            }
        };
        mainHandler.post(myRunnable);
    }

    protected void hideLoading() {
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.hide();
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    public ControllerItem(Context context) {
        this(context, null);
    }

    public ControllerItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        mIconFont = FontManager.getTypeface(context, FontManager.PLANT_FACTORY_FONT);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ControllerItem, 0, 0);
        String iconCode = a.getString(R.styleable.ControllerItem_iconCode);
        String name = a.getString(R.styleable.ControllerItem_name);


        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_controller_item, this, true);

        FontManager.markAsIconContainer(this, mIconFont);

        mIcon = (TextView) findViewById(R.id.tv_icon);
        mError = (TextView) findViewById(R.id.tv_error);
        mName = (TextView) findViewById(R.id.tv_name);

        mName.setText(name);
        mIcon.setText(iconCode);

        mIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean nextValue = !ControllerItem.this.isOn;
                setValueForSwitcher(nextValue);
                updateAPIToValue();
            }
        });

//        mSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    mIcon.setTextColor(ContextCompat.getColor(context, R.color.colorTextPrimary));
//                }
//                else {
//                    mIcon.setTextColor(Color.GRAY);
//                }
//
//                if (isHumanGesture == false) {
//                    isHumanGesture = true;
//                    return;
//                }
//                doNotUpdateValueForThis = true;
//                showLoading();
//                PFService.getInstance().toggleController(mItem.controller_id + "", new PFService.Callback<Controller>() {
//                    @Override
//                    public void onSuccess(final Controller data) {
//                        Handler mainHandler = new Handler(context.getMainLooper());
//
//                        Runnable myRunnable = new Runnable() {
//                            @Override
//                            public void run() {
//                                hideLoading();
//                                doNotUpdateValueForThis = false;
//                                //mSwitcher.setChecked(data.controller_is_on);
//                                //notHumanGusture = true;
//                            }
//                        };
//                        mainHandler.post(myRunnable);
//                    }
//
//                    @Override
//                    public void onFail(String error) {
//                        Handler mainHandler = new Handler(context.getMainLooper());
//
//                        Runnable myRunnable = new Runnable() {
//                            @Override
//                            public void run() {
//                                hideLoading();
//                                doNotUpdateValueForThis = false;
//                                mSwitcher.setChecked(!mSwitcher.isChecked());
//                                Utils.showErrorDialog(mContext, mContext.getString(R.string.dialog_message_toggle_controller_fail));
//                            }
//                        };
//                        mainHandler.post(myRunnable);
//                    }
//                });
//            }
//        });
    }

    private void updateAPIToValue() {
        showLoading();
        PFService.getInstance().toggleController(mItem.controller_id + "", new PFService.Callback<Controller>() {
            @Override
            public void onSuccess(final Controller data) {
                Handler mainHandler = new Handler(mContext.getMainLooper());

                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                    }
                };
                mainHandler.post(myRunnable);
            }

            @Override
            public void onFail(String error) {
                Handler mainHandler = new Handler(mContext.getMainLooper());

                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        boolean nextValue = !ControllerItem.this.isOn;
                        setValueForSwitcher(nextValue);
                    }
                };
                mainHandler.post(myRunnable);
            }
        });
    }

    private void checkControllerError() {
        if (mItem == null) {
            return;
        }

        if (mItem.is_error) {
            mError.setVisibility(VISIBLE);
        }
        else {
            mError.setVisibility(GONE);
        }
    }

    public void setValueForSwitcher(boolean value) {
        if (doNotUpdateValueForThis) {
            return;
        }

        isOn = value;
        updateUI();
        checkControllerError();
    }

    private void updateUI() {
        int color = isOn ? R.color.colorTextPrimary : R.color.colorTextGray;
        mIcon.setTextColor(ContextCompat.getColor(mContext, color));
        mName.setTextColor(ContextCompat.getColor(mContext, color));
    }
}
