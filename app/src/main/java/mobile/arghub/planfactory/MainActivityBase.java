package mobile.arghub.planfactory;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import mobile.arghub.planfactory.add_farm.AddFarmActivity;
import mobile.arghub.planfactory.edit_profile.EditProfileActivity;
import mobile.arghub.planfactory.edit_profile.UpdateAvatarActivity;
import mobile.arghub.planfactory.introduction.IntroActivity;
import mobile.arghub.planfactory.new_season.NewSeasonActivity;
import mobile.arghub.planfactory.registration.PhoneRegistrationAvatarActivity;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.settings.SettingActivity;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;

public class MainActivityBase extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected final int UPDATE_PROFILE_KEY = 111;
    protected final int UPDATE_AVATAR_KEY = 222;
    protected final int ADD_FARM = 333;

    ImageView mAvatar;
    ImageButton mEditProfile;
    ImageButton mEditAvatar;
    ListView mNavigationList;
    TextView mNoFarmLable;
    Button mAddFarm;

    MenuItem mNewSeasonMenuItem;
    MenuItem mSettingsMenuItem;

    PFService mService = PFService.getInstance();
    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("");

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.app_name));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAvatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.iv_avatar);
        mEditProfile = (ImageButton) navigationView.getHeaderView(0).findViewById(R.id.ib_edit_profile);
        mEditAvatar = (ImageButton) navigationView.getHeaderView(0).findViewById(R.id.ib_change_avatar);
        mAddFarm = (Button) findViewById(R.id.btn_add_farm);
        mNavigationList = (ListView) findViewById(R.id.lv_farms);
        mNoFarmLable = (TextView) findViewById(R.id.tv_no_farms);

        updateAvatar();
        setupControlEvents();
    }

    void updateAvatar() {
        Bitmap avatar = mService.getCurrentAvatarBitmap(MainActivityBase.this);
        if (avatar != null) {
            mAvatar.setImageBitmap(Utils.getCircleBitmap(avatar));
            return;
        }

        Picasso.with(MainActivityBase.this).load(mService.currentUser.user_avatar).into(target);
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mAvatar.setImageBitmap(Utils.getCircleBitmap(bitmap));
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    void setupControlEvents() {
        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityBase.this, EditProfileActivity.class);
                startActivityForResult(intent, UPDATE_PROFILE_KEY);
            }
        });

        mEditAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityBase.this, UpdateAvatarActivity.class);
                startActivityForResult(intent, UPDATE_AVATAR_KEY);
            }
        });

        mAddFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityBase.this, AddFarmActivity.class);
                startActivityForResult(intent, ADD_FARM);
            }
        });
    }

    void closeMenu(boolean animated) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START, animated);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_FARM) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getStringExtra("success").equalsIgnoreCase("1")) {
                    String title = getString(R.string.app_name);
                    String mess = getString(R.string.add_farm_qr_code_success);
                    showDialog(title, mess);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mNewSeasonMenuItem = menu.findItem(R.id.action_new_season);
        mSettingsMenuItem = menu.findItem(R.id.action_settings);
        mSettingsMenuItem.setVisible(false);
        mNewSeasonMenuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            gotoSetting();
            return true;
        }
        else if (id == R.id.action_new_season) {
            addNewSeason();
            return true;
        }
        else if (id == R.id.action_log_out) {
            requestLogout();
        }

        return super.onOptionsItemSelected(item);
    }

    protected void gotoSetting() {
        //TODO
    }

    protected void addNewSeason() {
        //TODO
    }

    protected void requestLogout() {
        //TODO
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
