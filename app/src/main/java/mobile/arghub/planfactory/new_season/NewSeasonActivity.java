package mobile.arghub.planfactory.new_season;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.utils.BaseActivity;

interface NewSeasonInterface {
    void bindingControls();
    void setupControlEvents();
    void initData();
    void addSeason();
}

public class NewSeasonActivity extends BaseActivity implements NewSeasonInterface, DatePickerDialog.OnDateSetListener {

    private final int ADD_PLANT_KEY = 3546;

    Button mChosePlant;
    Button mChoseDate;
    EditText mPlantAge;
    Button mContinue;

    Calendar myCalendar = Calendar.getInstance();
    PFService mService = PFService.getInstance();
    Long mChoseDateTM = null;

    private String currentPlantID = null;
    private String currentFarmID = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_season_activity);

        bindingControls();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControls() {
        mChosePlant = (Button) findViewById(R.id.btn_add_plant);
        mChoseDate = (Button) findViewById(R.id.btn_choose_date);
        mPlantAge = (EditText) findViewById(R.id.et_plant_age);
        mContinue = (Button) findViewById(R.id.btn_continue);
    }

    @Override
    public void setupControlEvents() {
        mChosePlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewSeasonActivity.this, SelectPlantActivity.class);
                startActivityForResult(intent, ADD_PLANT_KEY);
            }
        });

        mChoseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(NewSeasonActivity.this, NewSeasonActivity.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSeason();
            }
        });
    }

    @Override
    public void initData() {
        currentFarmID = getIntent().getStringExtra("farmID");
    }

    @Override
    public void addSeason() {
        if (currentPlantID == null) {
            showErrorDialog(getString(R.string.new_season_plant_empty));
            return;
        }

        if (mChoseDateTM == null) {
            showErrorDialog(getString(R.string.new_season_date_empty));
            return;
        }

        String age = mPlantAge.getText().toString();
        if (age.isEmpty()) {
            showErrorDialog(getString(R.string.new_season_age_empty));
            return;
        }

        showLoading();
        mService.addSeason(currentFarmID, currentPlantID, mChoseDateTM + "", age, new PFService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                NewSeasonActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent();
                        intent.putExtra("success", "1");
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final String error) {
                NewSeasonActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(error);
                        return;
                    }
                });
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        this.mChoseDateTM = myCalendar.getTimeInMillis();
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        mChoseDate.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_PLANT_KEY) {
            if (resultCode == Activity.RESULT_OK) {
                currentPlantID = data.getStringExtra("id");
                String name = data.getStringExtra("name");
                mChosePlant.setText(name);
            }
        }
    }
}
