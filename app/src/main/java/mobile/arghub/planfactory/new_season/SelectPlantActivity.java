package mobile.arghub.planfactory.new_season;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.adapter.PlantsAdapter;
import mobile.arghub.planfactory.service.model.Plant;
import mobile.arghub.planfactory.service.model.PlantItemDetail;
import mobile.arghub.planfactory.service.response_model.GetPlantResponse;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.Utils;

interface SelectPlantInterface {
    void bindingControls();
    void setupControlEvents();
    void initData();
}

public class SelectPlantActivity extends BaseActivity implements SelectPlantInterface {

    SearchView mSearchView;
    ListView mPlantList;

    PFService mService = PFService.getInstance();

    PlantsAdapter mAdapter;
    Gson gson = new Gson();
    List<Plant> mSearchCollection = new ArrayList<>();
    List<Plant> mOriginalCollection;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_plant_activity);

        bindingControls();
        setupControlEvents();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        showLoading();
        mService.getPlants(new PFService.Callback<GetPlantResponse>() {
            @Override
            public void onSuccess(final GetPlantResponse data) {
                SelectPlantActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        if (data == null) {
                            mOriginalCollection = null;
                            if (mAdapter != null) {
                                mAdapter.clear();
                                mAdapter.notifyDataSetChanged();
                            }
                            return;
                        }

                        mOriginalCollection = cloneList(data.plants);

                        if (mAdapter == null) {
                            mAdapter = new PlantsAdapter(SelectPlantActivity.this, data.plants);
                            mPlantList.setAdapter(mAdapter);
                            return;
                        }

                        mAdapter.clear();
                        mAdapter.addAll(data.plants);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                SelectPlantActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.select_plant_get_fail));
                        finish();
                    }
                });
            }
        });
    }

    @Override
    public void bindingControls() {
        mSearchView = (SearchView) findViewById(R.id.sv_search_view);
        mPlantList = (ListView) findViewById(R.id.lv_plant_list);
    }

    @Override
    public void setupControlEvents() {
        mPlantList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userMayChoseItem(position);
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText == null || newText.isEmpty()) {
                    mAdapter.clear();
                    mAdapter.addAll(mOriginalCollection);
                    mAdapter.notifyDataSetChanged();
                    return false;
                }
                mSearchCollection.clear();
                for (Plant plant : mOriginalCollection) {
                    try {
                        PlantItemDetail names = gson.fromJson(plant.plant_name, PlantItemDetail.class);
                        String name = URLDecoder.decode(names.getValue(), "UTF-8");
                        if (name.toLowerCase().contains(newText.toLowerCase())) {
                            mSearchCollection.add(plant.clone());
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                mAdapter.clear();
                mAdapter.addAll(mSearchCollection);
                mAdapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    @Override
    public void initData() {
        mSearchView.setIconified(false);
    }

    private void userMayChoseItem(int pos) {
        mSearchView.clearFocus();
        try {
            final Plant plant = mAdapter.getItem(pos);
            PlantItemDetail names = gson.fromJson(plant.plant_name, PlantItemDetail.class);
            final String name = URLDecoder.decode(names.getValue(), "UTF-8");

            Intent intent = new Intent();
            intent.putExtra("id", plant.plant_id + "");
            intent.putExtra("name", name);
            setResult(Activity.RESULT_OK, intent);
            finish();
//
//            String title = getString(R.string.app_name);
//            String message = getString(R.string.select_plant_message) + " " + name + "?";
//            final AlertDialog dialog = new AlertDialog.Builder(SelectPlantActivity.this)
//                    .setTitle(title)
//                    .setMessage(message)
//                    .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Utils.hideKeyboard(SelectPlantActivity.this);
//                            dialog.dismiss();
//                        }
//                    })
//                    .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            Intent intent = new Intent();
//                            intent.putExtra("id", plant.plant_id + "");
//                            intent.putExtra("name", name);
//                            setResult(Activity.RESULT_OK, intent);
//                            dialogInterface.dismiss();
//                            finish();
//                        }
//                    }).create();
//            if (!dialog.isShowing()) {
//                dialog.show();
//            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static List<Plant> cloneList(List<Plant> list) {
        List<Plant> clone = new ArrayList<Plant>(list.size());
        for (Plant item : list) clone.add(item.clone());
        return clone;
    }
}
