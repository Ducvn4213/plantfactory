package mobile.arghub.planfactory.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import mobile.arghub.planfactory.MainActivity;
import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.service.model.Setting;
import mobile.arghub.planfactory.service.network.Param;
import mobile.arghub.planfactory.utils.BaseActivity;
import mobile.arghub.planfactory.utils.SettingRangeItem;
import mobile.arghub.planfactory.utils.SettingSetCronTimeItem;
import mobile.arghub.planfactory.utils.SettingSetTimeItem;
import mobile.arghub.planfactory.utils.SettingSingleValueEditItem;
import mobile.arghub.planfactory.utils.SettingSingleValueItem;

public class SettingActivity extends BaseActivity {

    SettingRangeItem mCo2SettingItem;
    SettingRangeItem mPHSettingItem;
    SettingRangeItem mECSettingItem;
    SettingRangeItem mWaterSettingItem;
    SettingRangeItem mTempSettingItem;

    SettingSingleValueItem mECASettingItem;
    SettingSingleValueItem mECBSettingItem;
    SettingSingleValueItem mECCSettingItem;

    SettingSetCronTimeItem mWashingSettingItem;

    SettingSetTimeItem mWaterPumpSettingItem;
    SettingSetTimeItem mOxygenPumpSettingItem;
    SettingSetTimeItem mACSettingItem;
    SettingSetTimeItem mLampSettingItem;

    SettingSingleValueEditItem mWaterVolumeSettingItem;
    SettingSingleValueEditItem mLiquidVolumeSettingItem;
    SettingSingleValueItem mTankHeightSettingItem;
    SettingSingleValueItem mSensorHeightSettingItem;

    String currentFarmID;
    Setting currentData;

    PFService mService = PFService.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bindingControls();
        setupControlEvents();
        initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            confirmSave();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void confirmSave() {
        final AlertDialog dialog = new AlertDialog.Builder(SettingActivity.this)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.setting_save_confirm_message))
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        doSaveChanged();
                        dialog.dismiss();
                    }
                }).create();

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    void doSaveChanged() {
        List<List<Param>> updateValues = new ArrayList<>();
        //get all of value
        int co2_min = mCo2SettingItem.getCurrentMinValue();
        int co2_max = mCo2SettingItem.getCurrentMaxValue();
        int old_co2_min = (int) currentData.factory_co2_min;
        int old_co2_max = (int) currentData.factory_co2_max;
        if (co2_min != old_co2_min || co2_max != old_co2_max) {
            List<Param> co2_param = new ArrayList<>();
            co2_param.add(new Param("factory_co2_min", co2_min + ""));
            co2_param.add(new Param("factory_co2_max", co2_max + ""));

            updateValues.add(co2_param);
        }

        double ph_min = mPHSettingItem.getDoubleCurrentMinValue();
        double ph_max = mPHSettingItem.getDoubleCurrentMaxValue();
        double old_ph_min = currentData.factory_ph_min;
        double old_ph_max = currentData.factory_ph_max;
        if (ph_min != old_ph_min || ph_max != old_ph_max) {
            List<Param> ph_param = new ArrayList<>();
            ph_param.add(new Param("factory_ph_min", ph_min + ""));
            ph_param.add(new Param("factory_ph_max", ph_max + ""));

            updateValues.add(ph_param);
        }

        double ec_min = mECSettingItem.getDoubleCurrentMinValue();
        double ec_max = mECSettingItem.getDoubleCurrentMaxValue();
        double old_ec_min = currentData.factory_ec_min;
        double old_ec_max = currentData.factory_ec_max;
        if (ec_min != old_ec_min || ec_max != old_ec_max) {
            List<Param> ec_param = new ArrayList<>();
            ec_param.add(new Param("factory_ec_min", ec_min + ""));
            ec_param.add(new Param("factory_ec_max", ec_max + ""));

            updateValues.add(ec_param);
        }

        int water_min = mWaterSettingItem.getCurrentMinValue();
        int water_max = mWaterSettingItem.getCurrentMaxValue();
        int old_water_min = (int) currentData.factory_water_level_min;
        int old_water_max = (int) currentData.factory_water_level_max;
        if (water_min != old_water_min || water_max != old_water_max) {
            List<Param> water_param = new ArrayList<>();
            water_param.add(new Param("factory_water_level_min", water_min + ""));
            water_param.add(new Param("factory_water_level_max", water_max + ""));

            updateValues.add(water_param);
        }

        int temp_min = mTempSettingItem.getCurrentMinValue();
        int temp_max = mTempSettingItem.getCurrentMaxValue();
        int old_temp_min = (int) currentData.factory_temperature_level_min;
        int old_temp_max = (int) currentData.factory_temperature_level_max;
        if (temp_min != old_temp_min || temp_max != old_temp_max) {
            List<Param> temp_param = new ArrayList<>();
            temp_param.add(new Param("factory_temperature_level_min", temp_min + ""));
            temp_param.add(new Param("factory_temperature_level_max", temp_max + ""));

            updateValues.add(temp_param);
        }


        int eca = mECASettingItem.getCurrentValue();
        int old_eca = (int) currentData.factory_ec_formula_a;
        if (eca != old_eca) {
            List<Param> eca_param = new ArrayList<>();
            eca_param.add(new Param("factory_ec_formula_a", eca + ""));

            updateValues.add(eca_param);
        }

        int ecb = mECBSettingItem.getCurrentValue();
        int old_ecb = (int) currentData.factory_ec_formula_b;
        if (ecb != old_ecb) {
            List<Param> ecb_param = new ArrayList<>();
            ecb_param.add(new Param("factory_ec_formula_b", ecb + ""));

            updateValues.add(ecb_param);
        }

        int ecc = mECCSettingItem.getCurrentValue();
        int old_ecc = (int) currentData.factory_ec_formula_c;
        if (ecc != old_ecc) {
            List<Param> ecc_param = new ArrayList<>();
            ecc_param.add(new Param("factory_ec_formula_c", ecc + ""));

            updateValues.add(ecc_param);
        }

        String washing = mWashingSettingItem.getCurrentCronValue();
        String old_washing = currentData.factory_washing_mode_timer;
        if (!washing.equalsIgnoreCase(old_washing)) {
            List<Param> washing_param = new ArrayList<>();
            washing_param.add(new Param("factory_washing_mode_timer", washing));

            updateValues.add(washing_param);
        }

        long water_start_time = mWaterPumpSettingItem.getCurrentStartTimte();
        long water_end_time = mWaterPumpSettingItem.getCurrentEndTime();
        long old_water_start_time = currentData.factory_water_pump_started_time;
        long old_water_end_time = currentData.factory_water_pump_stopped_time;
        if (water_start_time != old_water_start_time || water_end_time != old_water_end_time) {
            List<Param> water_time_param = new ArrayList<>();
            water_time_param.add(new Param("factory_water_pump_started_time", water_start_time + ""));
            water_time_param.add(new Param("factory_water_pump_stopped_time", water_end_time + ""));

            updateValues.add(water_time_param);
        }

        long oxygen_start_time = mOxygenPumpSettingItem.getCurrentStartTimte();
        long oxygen_end_time = mOxygenPumpSettingItem.getCurrentEndTime();
        long old_oxygen_start_time = currentData.factory_oxygen_pump_started_time;
        long old_oxygen_end_time = currentData.factory_oxygen_pump_stopped_time;
        if (oxygen_start_time != old_oxygen_start_time || oxygen_end_time != old_oxygen_end_time) {
            List<Param> oxygen_time_param = new ArrayList<>();
            oxygen_time_param.add(new Param("factory_oxygen_pump_started_time", oxygen_start_time + ""));
            oxygen_time_param.add(new Param("factory_oxygen_pump_stopped_time", oxygen_end_time + ""));

            updateValues.add(oxygen_time_param);
        }

        long ac_start_time = mACSettingItem.getCurrentStartTimte();
        long ac_end_time = mACSettingItem.getCurrentEndTime();
        long old_ac_start_time = currentData.factory_ac_started_time;
        long old_ac_end_time = currentData.factory_ac_stopped_time;
        if (ac_start_time != old_ac_start_time || ac_end_time != old_ac_end_time) {
            List<Param> ac_time_param = new ArrayList<>();
            ac_time_param.add(new Param("factory_ac_started_time", ac_start_time + ""));
            ac_time_param.add(new Param("factory_ac_stopped_time", ac_end_time + ""));

            updateValues.add(ac_time_param);
        }

        long lamp_start_time = mLampSettingItem.getCurrentStartTimte();
        long lamp_end_time = mLampSettingItem.getCurrentEndTime();
        long old_lamp_start_time = currentData.factory_lamp_started_time;
        long old_lamp_end_time = currentData.factory_lamp_stopped_time;
        if (lamp_start_time != old_lamp_start_time || lamp_end_time != old_lamp_end_time) {
            List<Param> lamp_time_param = new ArrayList<>();
            lamp_time_param.add(new Param("factory_lamp_started_time", lamp_start_time + ""));
            lamp_time_param.add(new Param("factory_lamp_stopped_time", lamp_end_time + ""));

            updateValues.add(lamp_time_param);
        }

        int waterVolume = mWaterVolumeSettingItem.getCurrentValue();
        int oldWaterVolume = currentData.factory_water_volume;
        if (waterVolume != oldWaterVolume) {
            List<Param> water_volume_param = new ArrayList<>();
            water_volume_param.add(new Param("factory_water_volume", waterVolume + ""));

            updateValues.add(water_volume_param);
        }

        int liquidVolume = mLiquidVolumeSettingItem.getCurrentValue();
        int oldLiquidVolume = currentData.factory_liquid_volume;
        if (liquidVolume != oldLiquidVolume) {
            List<Param> liquid_volume_param = new ArrayList<>();
            liquid_volume_param.add(new Param("factory_liquid_volume", liquidVolume + ""));

            updateValues.add(liquid_volume_param);
        }

        int tankHeight = mTankHeightSettingItem.getCurrentValue();
        int oldTankHeight = currentData.factory_tank_height;
        if (tankHeight != oldTankHeight) {
            List<Param> tank_height_param = new ArrayList<>();
            tank_height_param.add(new Param("factory_tank_height", tankHeight + ""));

            updateValues.add(tank_height_param);
        }

        int sensorHeight = mSensorHeightSettingItem.getCurrentValue();
        int oldSensorHeight = currentData.factory_sensor_height;
        if (sensorHeight != oldSensorHeight) {
            List<Param> sensor_height_param = new ArrayList<>();
            sensor_height_param.add(new Param("factory_sensor_height", sensorHeight + ""));

            updateValues.add(sensor_height_param);
        }

        showLoading();
        mService.saveSettings(currentData.factory_id + "", updateValues, new PFService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        finish();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                SettingActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            showLoading();
            mService.getSettings(currentFarmID, new PFService.Callback<Setting>() {
                @Override
                public void onSuccess(final Setting data) {
                    SettingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            updateValue(data);
                        }
                    });
                }

                @Override
                public void onFail(String error) {
                    SettingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            showErrorDialog(getString(R.string.setting_load_data_fail));
                        }
                    });
                }
            });
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void updateValue(Setting data) {
        currentData = data;
        mCo2SettingItem.setCurrentMinValue((int) data.factory_co2_min);
        mCo2SettingItem.setCurrentMaxValue((int) data.factory_co2_max);

        mPHSettingItem.setCurrentMinValue(data.factory_ph_min);
        mPHSettingItem.setCurrentMaxValue(data.factory_ph_max);

        mECSettingItem.setCurrentMinValue(data.factory_ec_min);
        mECSettingItem.setCurrentMaxValue(data.factory_ec_max);

        mWaterSettingItem.setCurrentMinValue((int) data.factory_water_level_min);
        mWaterSettingItem.setCurrentMaxValue((int) data.factory_water_level_max);

        mTempSettingItem.setCurrentMinValue((int) data.factory_temperature_level_min);
        mTempSettingItem.setCurrentMaxValue((int) data.factory_temperature_level_max);

        mECASettingItem.setCurrentValue((int) data.factory_ec_formula_a);
        mECBSettingItem.setCurrentValue((int) data.factory_ec_formula_b);
        mECCSettingItem.setCurrentValue((int) data.factory_ec_formula_c);

        mWashingSettingItem.setValue(data.factory_washing_mode_timer);

        mWaterPumpSettingItem.setStartTime(data.factory_water_pump_started_time);
        mWaterPumpSettingItem.setEndTime(data.factory_water_pump_stopped_time);

        mOxygenPumpSettingItem.setStartTime(data.factory_oxygen_pump_started_time);
        mOxygenPumpSettingItem.setEndTime(data.factory_oxygen_pump_stopped_time);

        mACSettingItem.setStartTime(data.factory_ac_started_time);
        mACSettingItem.setEndTime(data.factory_ac_stopped_time);

        mLampSettingItem.setStartTime(data.factory_lamp_started_time);
        mLampSettingItem.setEndTime(data.factory_lamp_stopped_time);

        mWaterVolumeSettingItem.setValue(data.factory_water_volume);
        mLiquidVolumeSettingItem.setValue(data.factory_liquid_volume);
        mTankHeightSettingItem.setCurrentValue(data.factory_tank_height);
        mSensorHeightSettingItem.setCurrentValue(data.factory_sensor_height);
    }

    void bindingControls() {
        mCo2SettingItem = (SettingRangeItem) findViewById(R.id.sri_co2);
        mPHSettingItem = (SettingRangeItem) findViewById(R.id.sri_ph);
        mECSettingItem = (SettingRangeItem) findViewById(R.id.sri_ec);
        mWaterSettingItem = (SettingRangeItem) findViewById(R.id.sri_water_level);
        mTempSettingItem = (SettingRangeItem) findViewById(R.id.sri_temp);

        mECASettingItem = (SettingSingleValueItem) findViewById(R.id.ssvi_ec_a);
        mECBSettingItem = (SettingSingleValueItem) findViewById(R.id.ssvi_ec_b);
        mECCSettingItem = (SettingSingleValueItem) findViewById(R.id.ssvi_ec_c);

        mWashingSettingItem = (SettingSetCronTimeItem) findViewById(R.id.sscti_washing_time);

        mWaterPumpSettingItem = (SettingSetTimeItem) findViewById(R.id.ssti_water_pump);
        mOxygenPumpSettingItem = (SettingSetTimeItem) findViewById(R.id.ssti_oxygen_pump);
        mACSettingItem = (SettingSetTimeItem) findViewById(R.id.ssti_ac);
        mLampSettingItem = (SettingSetTimeItem) findViewById(R.id.ssti_lamp);

        mWaterVolumeSettingItem = (SettingSingleValueEditItem) findViewById(R.id.ssvei_water_volume);
        mLiquidVolumeSettingItem = (SettingSingleValueEditItem) findViewById(R.id.ssvei_liquid_volume);
        mTankHeightSettingItem = (SettingSingleValueItem) findViewById(R.id.ssvi_tank_height);
        mSensorHeightSettingItem = (SettingSingleValueItem) findViewById(R.id.ssvi_sensor_height);
    }

    void setupControlEvents() {

    }

    void initData() {
        currentFarmID = getIntent().getStringExtra("farmID");

        mCo2SettingItem.setRange(0, 10000);
        mPHSettingItem.setRange(0, 14.0);
        mECSettingItem.setRange(0, 20.0);
        mWaterSettingItem.setRange(0, 100);
        mTempSettingItem.setRange(0, 40);

        mECASettingItem.setRange(0, 100);
        mECBSettingItem.setRange(0, 100);
        mECCSettingItem.setRange(0, 100);

        mTankHeightSettingItem.setRange(21, 399);
        mSensorHeightSettingItem.setRange(1, 99);
    }
}
