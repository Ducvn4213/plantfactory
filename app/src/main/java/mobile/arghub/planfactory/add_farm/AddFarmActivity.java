package mobile.arghub.planfactory.add_farm;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.network.Config;
import mobile.arghub.planfactory.utils.BaseActivity;

interface AddFarmInterface {
    void bindingControl();
    void setupControlEvents();
    void initData();
}

public class AddFarmActivity extends BaseActivity implements AddFarmInterface{

    private final int REQUEST_CODE = 3456;
    Button mContinue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_farm_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    @Override
    public void bindingControl() {
        mContinue = (Button) findViewById(R.id.btn_continue);
    }

    @Override
    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission();
                //Intent intent = new Intent(AddFarmActivity.this, AddFarmQRCodeActivity.class);
                //startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    void checkCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(AddFarmActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            gotoCameraQRCodeScreen();
        }
        else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(AddFarmActivity.this,
                    perArr,
                    Config.PERMISSIONS_CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Config.PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    gotoCameraQRCodeScreen();
                }
                else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                }
            }
            else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
            }
        }
    }

    private void gotoCameraQRCodeScreen() {
        Intent intent = new Intent(AddFarmActivity.this, AddFarmQRCodeActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void initData() {
        //DO NOTHING
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getStringExtra("success").equalsIgnoreCase("1")) {
                    setResult(resultCode, data);
                    finish();
                }
            }
        }
    }
}
