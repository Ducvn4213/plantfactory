package mobile.arghub.planfactory.add_farm;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.PFService;
import mobile.arghub.planfactory.utils.BaseActivity;

public class AddFarmQRCodeActivity extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private QRCodeReaderView qrCodeReaderView;
    AlertDialog dialog;
    Boolean isProcessAddFarm = false;
    String processingFarmID = null;

    PFService mService = PFService.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_farm_qr_code_activity);

        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setQRDecodingEnabled(true);
        qrCodeReaderView.setAutofocusInterval(2000L);
        qrCodeReaderView.setTorchEnabled(true);

        //qrCodeReaderView.setFrontCamera();
        qrCodeReaderView.setBackCamera();
    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed in View
    @Override
    public void onQRCodeRead(final String text, PointF[] points) {
        if (isProcessAddFarm == true) {
            return;
        }
        String message = getString(R.string.add_farm_qr_code_found) + text + getString(R.string.add_farm_qr_code_message);

        if (dialog == null || dialog.isShowing() == false) {
            dialog = new AlertDialog.Builder(AddFarmQRCodeActivity.this)
                    .setTitle(getString(R.string.app_name))
                    .setMessage(message)
                    .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            isProcessAddFarm = true;
                            addFarm(text);
                            dialog.dismiss();
                        }
                    }).create();

            if (!dialog.isShowing()) {
                dialog.show();
            }
        }

        if (dialog.isShowing()) {
            return;
        }

    }

    void addFarm(String id) {
        processingFarmID = id;
        showLoading();
        getCurrentLocationAndContinue();
    }

    @Override
    protected void afterGetCurrentLocationFail() {
        super.afterGetCurrentLocationFail();
        hideLoading();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void afterGotCurrentLocation(double lat, double lon) {
        super.afterGotCurrentLocation(lat, lon);
        mService.addFarm(processingFarmID, lat + "" ,lon + "", new PFService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                AddFarmQRCodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("success", "1");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                AddFarmQRCodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        isProcessAddFarm = false;
                        processingFarmID = null;
                        showErrorDialog(getString(R.string.add_farm_qr_code_fail));
                    }
                });
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }
}
