package mobile.arghub.planfactory.service.response_model;

import java.util.List;

import mobile.arghub.planfactory.service.model.Farm;
import mobile.arghub.planfactory.service.model.Plant;

public class GetPlantResponse {
    public List<Plant> plants;

    public GetPlantResponse() {}
}
