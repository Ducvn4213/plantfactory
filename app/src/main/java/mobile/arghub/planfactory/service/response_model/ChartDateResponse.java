package mobile.arghub.planfactory.service.response_model;

import java.util.List;

import mobile.arghub.planfactory.service.model.SensorChartPointItem;

public class ChartDateResponse {
    public List<SensorChartPointItem> data;
    public long percent_liquid_a;
    public long percent_liquid_b;
    public long percent_liquid_c;
    public long percent_liquid_d;
    public long factory_id;

    public ChartDateResponse() {}
}
