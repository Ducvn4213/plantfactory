package mobile.arghub.planfactory.service;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.SettingInjectorService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.solver.Goal;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.internal.Excluder;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.registration.PhoneRegistrationAvatarActivity;
import mobile.arghub.planfactory.service.model.Controller;
import mobile.arghub.planfactory.service.model.Farm;
import mobile.arghub.planfactory.service.model.Nation;
import mobile.arghub.planfactory.service.model.Plant;
import mobile.arghub.planfactory.service.model.PlantItemDetail;
import mobile.arghub.planfactory.service.model.SensorChartPointItem;
import mobile.arghub.planfactory.service.model.Setting;
import mobile.arghub.planfactory.service.model.User;
import mobile.arghub.planfactory.service.network.Config;
import mobile.arghub.planfactory.service.network.Network;
import mobile.arghub.planfactory.service.network.Param;
import mobile.arghub.planfactory.service.network.Response;
import mobile.arghub.planfactory.service.response_model.ChartDateResponse;
import mobile.arghub.planfactory.service.response_model.GetFarmResponse;
import mobile.arghub.planfactory.service.response_model.GetPlantResponse;
import mobile.arghub.planfactory.service.response_model.NationResponse;
import mobile.arghub.planfactory.service.response_model.VerifyCodeResponse;
import mobile.arghub.planfactory.utils.Utils;

public class PFService {
    public interface Callback<E> {
        void onSuccess(E data);
        void onFail(String error);
    }

    private static PFService instance;
    public static PFService getInstance() {
        if (instance == null) {
            instance = new PFService();
        }

        return instance;
    }

    private Network mNetwork = Network.getInstance();

    public User currentUser;
    public String currentAccessToken;

    private List<Plant> mPlantList;

    private PFService() {}

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void getPassCode(String phoneNumber, final Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("phone", phoneNumber));

        mNetwork.execute(Config.PASS_CODE_GET, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess("200");
            }

            @Override
            public void onFail(String error) {
                callback.onFail("not 200");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void verifyCode(final Context context, String phone, String code, String lat, String lon, final Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("phone", phone));
        params.add(new Param("passcode", code));
        params.add(new Param("user_latitude", lat));
        params.add(new Param("user_longitude", lon));

        mNetwork.execute(Config.PASS_CODE_VERIFY, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                try {
                    VerifyCodeResponse verifyCodeResponse = gson.fromJson(response, VerifyCodeResponse.class);
                    String userString = verifyCodeResponse.userString;

                    User user = gson.fromJson(userString, User.class);
                    currentUser = user;

                    Utils.savePreference(context, Config.USER_KEY, userString);
                    Utils.savePreference(context, Config.USER_TOKEN_KEY, verifyCodeResponse.user_token);
                    currentAccessToken = verifyCodeResponse.user_token;

                    callback.onSuccess(verifyCodeResponse.is_new_user ? "1" : "0");
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail(ex.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail("not 200");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void updateProfile(final Context context, String firstName, String lastName, String email, String birth, String gender, final Callback<User> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("user_phone", currentUser.user_phone));
        params.add(new Param("user_first_name", firstName));
        params.add(new Param("user_last_name", lastName));
        params.add(new Param("user_birth_year", birth));
        params.add(new Param("user_gender", gender));
        params.add(new Param("user_email", email));

        String link = Config.UPDATE_PROFILE + Utils.decode(currentAccessToken);
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                try {
                    User user = gson.fromJson(response, User.class);
                    Utils.savePreference(context, Config.USER_KEY, response);

                    currentUser = user;
                    callback.onSuccess(user);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail("the response is invalid format");
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail("not 200");
            }
        });
    }

    public void updateAvatar(Bitmap newAvatar, final Callback<String> callback) {
        Utils.ConvertBitmapToStringTask convertBitmapToStringTask = new Utils.ConvertBitmapToStringTask();
        convertBitmapToStringTask.setBitmap(newAvatar);
        convertBitmapToStringTask.setCallback(new Utils.ConvertBitmapToStringCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSuccess(String bitmapString) {
                doUpdateAvatar(bitmapString, callback);
            }

            @Override
            public void onFail() {
                callback.onFail("The input image is not valid");
            }
        });
        convertBitmapToStringTask.execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void doUpdateAvatar(final String bitmapString, final Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("user_phone", currentUser.user_phone));
        params.add(new Param("user_avatar", bitmapString));

        String link = Config.UPDATE_AVATAR + Utils.decode(currentAccessToken);
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(bitmapString);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void getFarm(final Callback<GetFarmResponse> callback) {
        String link = Config.GET_FARM + Utils.decode(currentAccessToken) + "&id=" + currentAccessToken;
        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<Farm> farms = gson.fromJson(response, new TypeToken<List<Farm>>(){}.getType());

                List<Farm> reFarms = new ArrayList<Farm>();
                for (Farm f : farms) {
                    if (f.farm_type == 7) {
                        reFarms.add(f);
                    }
                }

                GetFarmResponse getFarmResponse = new GetFarmResponse();
                getFarmResponse.farms = reFarms;
                callback.onSuccess(getFarmResponse);
            }

            @Override
            public void onFail(String error) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void addFarm(String id, String lat, String lon, final Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_id", id));
        params.add(new Param("farm_latitude", lat));
        params.add(new Param("farm_longitude", lon));

        String link = Config.ACTIVATE_CONTAINER + Utils.decode(currentAccessToken);
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getPlants(final Callback<GetPlantResponse> callback) {
        String link = Config.GET_PLANT + Utils.decode(currentAccessToken);
        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<Plant> plants = gson.fromJson(response, new TypeToken<List<Plant>>(){}.getType());
                GetPlantResponse getPlantResponse = new GetPlantResponse();
                getPlantResponse.plants = plants;
                mPlantList = plants;
                callback.onSuccess(getPlantResponse);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getNations(final Callback<NationResponse> callback) {
        String link = Config.GET_NATION;
        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<Nation> nations = gson.fromJson(response, new TypeToken<List<Nation>>(){}.getType());
                NationResponse getNationResponse = new NationResponse();
                getNationResponse.data = nations;
                callback.onSuccess(getNationResponse);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void addSeason(String farmID, String seasonID, String date, String age, final Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("plant_id", seasonID));
        params.add(new Param("farm_id", farmID));
        params.add(new Param("season_start_date", date));
        params.add(new Param("season_age_of_plant", age));

        String link = Config.ADD_SEASON + Utils.decode(currentAccessToken);
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void toggleController(String id, final Callback<Controller> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("id", id));

        String link = Config.TOGGLE_CONTROLLER + Utils.decode(currentAccessToken);
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                try {
                    Gson gson = new Gson();
                    Controller controller = gson.fromJson(response, Controller.class);
                    callback.onSuccess(controller);
                }
                catch (Exception ex) {
                    callback.onFail("response is invalid");
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getSensorChartData(String sensorID, String dairy, final Callback<ChartDateResponse> callback) {
        String sensorIDURL = "&id=" + Utils.encode(sensorID);
        String dairyURL = "&diary_type=" + Utils.encode(dairy);
        String dairyPageURL = "&diary_page=MA==";
        String link = Config.GET_SENSOR + Utils.decode(currentAccessToken) + sensorIDURL + dairyURL + dairyPageURL;

        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<SensorChartPointItem> pointItems = gson.fromJson(response, new TypeToken<List<SensorChartPointItem>>(){}.getType());
                ChartDateResponse chartDateResponse = new ChartDateResponse();
                chartDateResponse.data = pointItems;
                callback.onSuccess(chartDateResponse);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getSensorChartDataWithLiquidValue(final String farmID, String sensorID, final String dairy, final Callback<ChartDateResponse> callback) {
        String sensorIDURL = "&id=" + Utils.encode(sensorID);
        String dairyURL = "&diary_type=" + Utils.encode(dairy);
        String dairyPageURL = "&diary_page=MA==";
        String link = Config.GET_SENSOR + Utils.decode(currentAccessToken) + sensorIDURL + dairyURL + dairyPageURL;

        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                final ChartDateResponse chartDateResponse = new ChartDateResponse();
                if (!response.equalsIgnoreCase("[]")) {
                    Gson gson = new Gson();
                    List<SensorChartPointItem> pointItems = gson.fromJson(response, new TypeToken<List<SensorChartPointItem>>(){}.getType());
                    chartDateResponse.data = pointItems;
                    //callback.onSuccess(chartDateResponse);
                }

                getSettings(farmID, new Callback<Setting>() {
                    @Override
                    public void onSuccess(Setting data) {
                        chartDateResponse.factory_id = data.factory_id;
                        chartDateResponse.percent_liquid_a = data.factory_liquid_level_a;
                        chartDateResponse.percent_liquid_b = data.factory_liquid_level_b;
                        chartDateResponse.percent_liquid_c = data.factory_liquid_level_c;
                        chartDateResponse.percent_liquid_d = data.factory_liquid_level_d;

                        callback.onSuccess(chartDateResponse);
                    }

                    @Override
                    public void onFail(String error) {
                        onFail(error);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getSettings(String farmID, final Callback<Setting> callback) {
        String farmIDURL = "&id=" + Utils.encode(farmID);
        String link = Config.GET_SETTING + Utils.decode(currentAccessToken) + farmIDURL;

        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                Setting setting = gson.fromJson(response, Setting.class);
                callback.onSuccess(setting);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void saveSettings(final String factoryID, final List<List<Param>> data, final Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        for (List<Param> lp : data) {
            params.addAll(lp);
        }

        saveSetting(factoryID, params, new Callback<String>() {
            @Override
            public void onSuccess(String _data) {
                callback.onSuccess("success");
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void saveSetting(String factoryID, List<Param> params, final Callback<String> callback) {
        params.add(new Param("factory_id", factoryID));

        String link = Config.SAVE_SETTING + Utils.decode(currentAccessToken);
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public Bitmap getCurrentAvatarBitmap(Context context) {
        if (currentUser.user_avatar != null && !currentUser.user_avatar.trim().isEmpty()) {
            return null;
        }
        String imageString = Utils.getPreference(context, Config.AVATAR_KEY);
        if (imageString == null) {
            Bitmap defaultAvatar = BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar);
            return defaultAvatar;
        }

        byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
        Bitmap avatar = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return avatar;
    }

    public String getPlantNameByID(long id) {
        if (mPlantList == null) {
            return id + "";
        }

        for (Plant p : mPlantList) {
            if (p.plant_id == id) {
                Gson gson = new Gson();
                PlantItemDetail names = gson.fromJson(p.plant_name, PlantItemDetail.class);
                try {
                    return URLDecoder.decode(names.getValue(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return id + "";
                }
            }
        }
        return id + "";
    }

    public String getPlanImageByID(long id) {
        if (mPlantList == null) {
            return null;
        }

        for (Plant p : mPlantList) {
            if (p.plant_id == id) {
                Gson gson = new Gson();
                List<String> images = gson.fromJson(p.plant_photos, new TypeToken<List<String>>(){}.getType());
                return  images.get(0);
            }
        }
        return null;
    }

    public void removeOldAvatar(Context context) {
        currentUser.user_avatar = null;
        Gson gson = new Gson();
        String userString = gson.toJson(currentUser);

        Utils.savePreference(context, Config.USER_KEY, userString);
    }
}
