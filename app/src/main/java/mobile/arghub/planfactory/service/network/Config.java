package mobile.arghub.planfactory.service.network;

public class Config {
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String USER_KEY = "USER_KEY";
    public static final String AVATAR_KEY = "AVATAR_KEY";
    public static final int PERMISSIONS_LOCATION_REQUEST = 8760;
    public static final int PERMISSIONS_CAMERA_REQUEST = 8761;

    private static final String HOST = "http://farmapi.agrhub.com";
    public static final String PASS_CODE_GET =  HOST + "/user/passcode/get";
    public static final String PASS_CODE_VERIFY =  HOST + "/user/passcode/verify";
    public static final String UPDATE_PROFILE =  HOST + "/user/edit?access_token=";
    public static final String UPDATE_AVATAR =  HOST + "/user/updateAvatar?access_token=";
    public static final String GET_FARM = HOST + "/farm/getFarmByUser?access_token=";
    public static final String ACTIVATE_CONTAINER = HOST + "/farm/activeContainer?access_token=";
    public static final String GET_PLANT = HOST + "/plant/gets?access_token=";
    public static final String ADD_SEASON = HOST + "/season/set?access_token=";
    public static final String TOGGLE_CONTROLLER = HOST + "/controller/toggle?access_token=";
    public static final String GET_SENSOR = HOST + "/sensor/get/series?access_token=";
    public static final String GET_NATION = HOST + "/nation/gets";
    public static final String GET_SETTING = HOST + "/profile/getFactoryByFarm?access_token=";
    public static final String SAVE_SETTING = HOST + "/profile/editFactory?access_token=";
}
