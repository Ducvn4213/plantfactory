package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class Sensor {
    @SerializedName("sensor_id")
    public long sensor_id;
    @SerializedName("sensor_type")
    public long sensor_type;
    @SerializedName("sensor_value")
    public double sensor_value;
    @SerializedName("is_error")
    public Boolean is_error;
    @SerializedName("sensor_registered_date")
    public long sensor_registered_date;
    @SerializedName("sensor_updated_date")
    public long sensor_updated_date;

    public long deviceName;
    public Sensor() {}
}
