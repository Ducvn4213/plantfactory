package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("user_phone")
    public String user_phone;
    @SerializedName("user_first_name")
    public String user_first_name;
    @SerializedName("user_last_name")
    public String user_last_name;
    @SerializedName("user_email")
    public String user_email;
    @SerializedName("user_avatar")
    public String user_avatar;
    @SerializedName("user_birthday")
    public long user_birthday;
    @SerializedName("user_gender")
    public int user_gender;
    @SerializedName("user_address")
    public String user_address;
    @SerializedName("user_latitude")
    public float user_latitude;
    @SerializedName("user_longitude")
    public float user_longitude;
    @SerializedName("city")
    public City city;
    @SerializedName("user_role")
    public int user_role;
    @SerializedName("user_membership")
    public int user_membership;
    @SerializedName("is_active")
    public Boolean is_active;
    @SerializedName("is_locked")
    public Boolean is_locked;
    @SerializedName("user_registered_date")
    public long user_registered_date;
    @SerializedName("user_updated_date")
    public long user_updated_date;

    public User() {}
}
