package mobile.arghub.planfactory.service.response_model;

import java.util.List;

import mobile.arghub.planfactory.service.model.Farm;

public class GetFarmResponse {
    public List<Farm> farms;

    public GetFarmResponse() {}
}
