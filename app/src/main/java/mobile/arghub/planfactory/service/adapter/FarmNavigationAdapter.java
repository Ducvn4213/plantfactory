package mobile.arghub.planfactory.service.adapter;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.widget.ArrayAdapter;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.model.Farm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class FarmNavigationAdapter extends ArrayAdapter<Farm> {

    private Context mContext;
    private List<Farm> mData;
    private static LayoutInflater mInflater = null;
    private int mSelectedItem = 0;

    public FarmNavigationAdapter(Context context, List<Farm> data) {
        super(context, R.layout.layout_farm_navigation_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public void setSelectedItem(int index) {
        mSelectedItem = index;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_farm_navigation_item, null);
        }

        TextView title = (TextView) view.findViewById(R.id.tv_title);
        title.setText(mData.get(position).farm_name);

        if (position == mSelectedItem) {
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            title.setTextColor(Color.WHITE);
        }
        else {
            view.setBackgroundColor(Color.TRANSPARENT);
            title.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        return view;
    }
}