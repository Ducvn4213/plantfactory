package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class Season {
    @SerializedName("season_id")
    public long season_id;
    @SerializedName("plant_id")
    public long plant_id;
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("season_age_of_plant")
    public long season_age_of_plant;
    @SerializedName("season_start_date")
    public long season_state_date;
    @SerializedName("season_quantity")
    public long season_quantity;
    @SerializedName("season_registered_date")
    public long season_registered_date;
    @SerializedName("season_updated_date")
    public long season_updated_date;

    public Season() {}
}
