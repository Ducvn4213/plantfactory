package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Device {
    @SerializedName("device_id")
    public long device_id;
    @SerializedName("device_name")
    public long device_name;
    @SerializedName("device_mac_address")
    public String device_mac_address;
    @SerializedName("user")
    public User user;
    @SerializedName("device_latitude")
    public float device_latitude;
    @SerializedName("device_longitude")
    public float device_longitude;
    @SerializedName("device_state")
    public long device_state;
    @SerializedName("is_active")
    public Boolean is_active;
    @SerializedName("is_block")
    public Boolean is_block;
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("device_type")
    public long device_type;
    @SerializedName("device_time_life")
    public long device_time_life;
    @SerializedName("device_registered_date")
    public long device_registered_date;
    @SerializedName("device_updated_date")
    public long device_updated_date;
    @SerializedName("sensors")
    public List<Sensor> sensors;
    @SerializedName("controllers")
    public List<Controller> controllers;


    public Device() {}
}
