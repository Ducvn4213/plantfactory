package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class Setting {
    @SerializedName("factory_id")
    public long factory_id;
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("factory_co2_min")
    public long factory_co2_min;
    @SerializedName("factory_co2_max")
    public long factory_co2_max;
    @SerializedName("factory_ph_min")
    public double factory_ph_min;
    @SerializedName("factory_ph_max")
    public double factory_ph_max;
    @SerializedName("factory_ec_min")
    public double factory_ec_min;
    @SerializedName("factory_ec_max")
    public double factory_ec_max;
    @SerializedName("factory_water_level_min")
    public long factory_water_level_min;
    @SerializedName("factory_water_level_max")
    public long factory_water_level_max;
    @SerializedName("factory_ec_formula_a")
    public long factory_ec_formula_a;
    @SerializedName("factory_ec_formula_b")
    public long factory_ec_formula_b;
    @SerializedName("factory_ec_formula_c")
    public long factory_ec_formula_c;
    @SerializedName("factory_washing_mode_timer")
    public String factory_washing_mode_timer;
    @SerializedName("factory_water_pump_started_time")
    public long factory_water_pump_started_time;
    @SerializedName("factory_water_pump_stopped_time")
    public long factory_water_pump_stopped_time;
    @SerializedName("factory_oxygen_pump_started_time")
    public long factory_oxygen_pump_started_time;
    @SerializedName("factory_oxygen_pump_stopped_time")
    public long factory_oxygen_pump_stopped_time;
    @SerializedName("factory_ac_started_time")
    public long factory_ac_started_time;
    @SerializedName("factory_ac_stopped_time")
    public long factory_ac_stopped_time;
    @SerializedName("factory_lamp_started_time")
    public long factory_lamp_started_time;
    @SerializedName("factory_lamp_stopped_time")
    public long factory_lamp_stopped_time;
    @SerializedName("factory_temperature_level_min")
    public long factory_temperature_level_min;
    @SerializedName("factory_temperature_level_max")
    public long factory_temperature_level_max;
    @SerializedName("factory_registered_date")
    public long factory_registered_date;
    @SerializedName("factory_updated_date")
    public long factory_updated_date;
    @SerializedName("factory_liquid_level_a")
    public long factory_liquid_level_a;
    @SerializedName("factory_liquid_level_b")
    public long factory_liquid_level_b;
    @SerializedName("factory_liquid_level_c")
    public long factory_liquid_level_c;
    @SerializedName("factory_liquid_level_d")
    public long factory_liquid_level_d;

    //new setting item
    @SerializedName("factory_water_volume")
    public int factory_water_volume;
    @SerializedName("factory_liquid_volume")
    public int factory_liquid_volume;
    @SerializedName("factory_tank_height")
    public int factory_tank_height;
    @SerializedName("factory_sensor_height")
    public int factory_sensor_height;

    public Setting() {}
}
