package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Farm {
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("farm_name")
    public String farm_name;
    @SerializedName("farm_address")
    public String farm_address;
    @SerializedName("farm_acreage")
    public long farm_acreage;
    @SerializedName("user_id")
    public long user_id;
    @SerializedName("farm_latitude")
    public float farm_latitude;
    @SerializedName("farm_longitude")
    public float farm_longitude;
    @SerializedName("farm_time_zone")
    public String farm_time_zone;
    @SerializedName("is_on_season")
    public Boolean is_on_season;
    @SerializedName("farm_time_life")
    public long farm_time_life;
    @SerializedName("farm_type")
    public long farm_type;
    @SerializedName("farm_registered_date")
    public long farm_registered_date;
    @SerializedName("farm_updated_date")
    public long farm_updated_date;
    @SerializedName("season")
    public Season season;
    @SerializedName("devices")
    public List<Device> devices;
    @SerializedName("cameras")
    public List<Camera> camera;


    public Farm() {}
}
