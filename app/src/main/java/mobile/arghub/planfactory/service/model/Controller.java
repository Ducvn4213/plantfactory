package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class Controller {
    @SerializedName("controller_id")
    public long controller_id;
    @SerializedName("controller_type")
    public long controller_type;
    @SerializedName("controller_value")
    public long controller_value;
    @SerializedName("controller_is_on")
    public Boolean controller_is_on;
    @SerializedName("controller_on_time")
    public long controller_on_time;
    @SerializedName("is_error")
    public Boolean is_error;
    @SerializedName("controller_registered_date")
    public long controller_registered_date;
    @SerializedName("controller_updated_date")
    public long controller_updated_date;
    @SerializedName("controller_last_manual_control_date")
    public long controller_last_manual_control_date;

    public Controller() {}
}

