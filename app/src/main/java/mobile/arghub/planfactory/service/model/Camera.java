package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class Camera {
    @SerializedName("camera_stream_uri")
    public String camera_stream_uri;

    public Camera() {}
}
