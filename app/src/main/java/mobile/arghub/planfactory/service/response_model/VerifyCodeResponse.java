package mobile.arghub.planfactory.service.response_model;

import com.google.gson.annotations.SerializedName;

import mobile.arghub.planfactory.service.model.User;

public class VerifyCodeResponse {
    @SerializedName("is_new_user")
    public Boolean is_new_user;
    @SerializedName("user_token")
    public String user_token;
    @SerializedName("user")
    public String userString;

    public VerifyCodeResponse() {}
}
