package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class City {
    @SerializedName("city_id")
    long city_id;
    @SerializedName("city_name")
    String city_name;
    @SerializedName("is_active")
    Boolean is_active;
    @SerializedName("nation")
    Nation nation;
    @SerializedName("city_registered_date")
    long city_registered_date;
    @SerializedName("city_updated_date")
    long city_updated_date;

    public City() {}
}
