package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

public class SensorChartPointItem {
    @SerializedName("sensor_diary_id")
    public long sensor_diary_id;
    @SerializedName("diary_date")
    public long diary_date;
    @SerializedName("sensor_id")
    public long sensor_id;
    @SerializedName("sensor_value")
    private Double sensor_value;
    @SerializedName("sensor_med")
    private Double sensor_med;

    public Double getValue() {
        if (sensor_value == null) {
            return sensor_med;
        }

        return sensor_value;
    }


    public SensorChartPointItem() {}
}
