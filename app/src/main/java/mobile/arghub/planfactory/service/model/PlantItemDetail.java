package mobile.arghub.planfactory.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.Locale;

public class PlantItemDetail {
    @SerializedName("vi")
    String vi;
    @SerializedName("en")
    String en;

    public String getValue() {
        if (Locale.getDefault().toString().contains("vi")) {
            return vi;
        }

        return en;
    }

    public PlantItemDetail() {}
}
