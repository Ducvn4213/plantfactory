package mobile.arghub.planfactory.service.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.service.model.Farm;
import mobile.arghub.planfactory.service.model.Plant;
import mobile.arghub.planfactory.service.model.PlantItemDetail;
import mobile.arghub.planfactory.utils.Utils;

public class PlantsAdapter extends ArrayAdapter<Plant> {

    private Context mContext;
    private List<Plant> mData;
    private static LayoutInflater mInflater = null;
    private int mSelectedItem = 0;
    Gson gson;

    public PlantsAdapter(Context context, List<Plant> data) {
        super(context, R.layout.layout_plant_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        gson = new Gson();
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public void setSelectedItem(int index) {
        mSelectedItem = index;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_plant_item, null);
        }

        Plant data = mData.get(position);

        final ImageView image = (ImageView) view.findViewById(R.id.iv_image);
        TextView title = (TextView) view.findViewById(R.id.tv_title);

        try {
            PlantItemDetail name = gson.fromJson(data.plant_name, PlantItemDetail.class);
            title.setText(URLDecoder.decode(name.getValue(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        List<String> images = gson.fromJson(data.plant_photos, new TypeToken<List<String>>(){}.getType());
        Picasso.with(mContext).load(images.get(0)).transform(new Utils.CircleTransform()).into(image);

        return view;
    }
}