package mobile.arghub.planfactory.service.response_model;

import java.util.List;
import mobile.arghub.planfactory.service.model.Nation;

public class NationResponse {
    public List<Nation> data;

    public NationResponse() {}
}
