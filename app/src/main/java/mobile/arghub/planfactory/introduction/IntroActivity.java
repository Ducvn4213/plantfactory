package mobile.arghub.planfactory.introduction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.github.paolorotolo.appintro.AppIntro;

import mobile.arghub.planfactory.MainActivityBase;
import mobile.arghub.planfactory.R;
import mobile.arghub.planfactory.introduction.content.IntroContentFiveFragment;
import mobile.arghub.planfactory.introduction.content.IntroContentFourFragment;
import mobile.arghub.planfactory.introduction.content.IntroContentOneFragment;
import mobile.arghub.planfactory.introduction.content.IntroContentSixFragment;
import mobile.arghub.planfactory.introduction.content.IntroContentThreeFragment;
import mobile.arghub.planfactory.introduction.content.IntroContentTwoFragment;
import mobile.arghub.planfactory.registration.PhoneRegistrationActivity;

public class IntroActivity extends AppIntro {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(new IntroContentOneFragment());
        addSlide(new IntroContentTwoFragment());
        addSlide(new IntroContentThreeFragment());
        addSlide(new IntroContentFourFragment());
        addSlide(new IntroContentFiveFragment());
        addSlide(new IntroContentSixFragment());

        setBarColor(Color.WHITE);
        setSeparatorColor(Color.WHITE);

        setIndicatorColor(Color.parseColor("#5abc35"), Color.GRAY);
        setColorDoneText(Color.parseColor("#5abc35"));
        setNextArrowColor(Color.parseColor("#5abc35"));



        setDoneText(getString(R.string.button_next));
        setProgressButtonEnabled(true);
        showSkipButton(false);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(IntroActivity.this, PhoneRegistrationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }
}
